#!/bin/bash
set -eo pipefail

export PATH=/opt/anaconda3/bin:$PATH
source activate kdatabase
gunicorn wsgi:app /home/bioinfo/tools/kdatabase/conf/app_exome.cfg --workers=4 --bind localhost:8888 --chdir /home/bioinfo/tools/kdatabase/app

