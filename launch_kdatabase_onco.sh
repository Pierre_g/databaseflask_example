#!/bin/bash
set -eo pipefail

export PATH=/opt/anaconda3/bin:$PATH
source activate kdatabase
gunicorn wsgi:app /home/bioinfo/tools/kdatabase/conf/app_onco.cfg --workers=1 --bind localhost:8090 --chdir /home/bioinfo/tools/kdatabase/app

