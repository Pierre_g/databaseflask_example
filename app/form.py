#!/usr/bin/env python3
# -*- coding: utf-8 -*-


from flask_wtf import FlaskForm
from wtforms import StringField, SubmitField,SelectField



class AnnotationForm(FlaskForm):
    """Compute form"""
    logiciel = SelectField("Logiciel",choices=[("sirius","Sirius"),
                                               ("niourk","Niourk2"),
                                               ("bulk","bulk")])
    operator = SelectField('Utilisateur',choices=[])
    run = SelectField('Run',choices=[])
    sample = SelectField("Sample",choices=[])  
    
    submit = SubmitField('Envoyer')



class GeneForm(FlaskForm):   
    recherche = StringField("Tapez votre recherche",
                            render_kw={"placeholder": "Tapez votre recherche"})
    
    freqgnomad_min = StringField("Freq. GnomADs",render_kw={"placeholder": "Min"})
    freqgnomad_max = StringField("Freq. GnomADs Max",render_kw={"placeholder": "Max"})
    freqCHU_min = StringField("Freq. CHU min",render_kw={"placeholder": "Min"},)
    freqCHU_max = StringField("Freq. CHU max",render_kw={"placeholder": "Max"})
    
    period = StringField("Période",render_kw={"placeholder": "Ex:2019-01-02/2020-06-03"})
    samplename = StringField('ID(s) echantillon',render_kw={"placeholder": "ID1 ou ID1/ID2"})
    
    vaf_htz_min = StringField("Freq. HTZ ",render_kw={"placeholder": "Min"})
    vaf_htz_max = StringField("Freq. HTZ max",render_kw={"placeholder": "Max"})
    vaf_hom_min = StringField("Freq. HOM",render_kw={"placeholder": "Min"})
    vaf_hom_max = StringField("Freq. HOM max",render_kw={"placeholder": "Max"})
    vaf_lowest_min = StringField("Freq. LOW",render_kw={"placeholder": "Min"})
    vaf_lowest_max = StringField("Freq. LOW max",render_kw={"placeholder": "Max"})
    
    occ_tot_min = StringField(render_kw={"placeholder": "Min"})
    occ_htz_min = StringField(render_kw={"placeholder": "Min"})
    occ_hom_min = StringField(render_kw={"placeholder": "Min"})
    occ_low_min = StringField(render_kw={"placeholder": "Min"})
    
    occ_tot_max = StringField(render_kw={"placeholder": "Max"})
    occ_htz_max = StringField(render_kw={"placeholder": "Max"})
    occ_hom_max = StringField(render_kw={"placeholder": "Max"})
    occ_low_max = StringField(render_kw={"placeholder": "Max"})
    
    download_request = SubmitField("Export",render_kw={"class":"button-27"})
    download_template = SubmitField("Export du modèle de requête",render_kw={"class":"button-27_template"})
    submit_all = SubmitField("Recherche")
 

    

