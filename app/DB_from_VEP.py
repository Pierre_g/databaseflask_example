#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from ipaddress import collapse_addresses
from multiprocessing import Pool
import numpy as np
from multiprocessing import Manager
import time
import os
import argparse
import vcf
from joblib import load,dump
import pandas as pd
from DB_connection import getSession
from DB_class import AssociationVariantGene, Base,Gene,\
    Annotation, Consequence,AssociationVariantAnnotation,AssociationVariantGene#, Session
import global_
from configparser import RawConfigParser
import tempfile as tmp
import sys
from tqdm import tqdm

def addConsequenceEntry(name,sessionVar):
    
    result = sessionVar.query(Consequence).filter(Consequence.consequence_name==name)

    if(result.count() == 0):
        c = Consequence(consequence_name=name)
        sessionVar.add(c)
        sessionVar.flush()
    
    sessionVar.commit()
    
    sessionVar.close()


def absentValue(data, col,sessionVarAbsVal):
    mask = data[col].str.contains(".*[a-z].*", na=False)
    df = data[mask]
    for elt in df[col].unique() :
        addConsequenceEntry(elt,sessionVar=sessionVarAbsVal)
    

def mapValueKey(data, dict_map, col,sessionVarMPK):
    data[col] = data[col].map(dict_map).fillna(data[col])
    #Si dtype objet => colonne mixe avec entier et str
    if(data[col].dtype == object):
        absentValue(data, col, sessionVarAbsVal=sessionVarMPK)
        return True
    else:
        return False
        
def checkIfGeneExist(subdata, df_gene):
    list_all_hgnc_id = []
    
    for items, row in subdata.iterrows():
        # list_hgnc_id = row['hgnc_id'].split(',')
        # for elt in list_hgnc_id:
        #     list_all_hgnc_id.append(elt.strip())
        list_all_hgnc_id.append(row['hgnc_id'])
              
    return(list(set(list_all_hgnc_id) - set(df_gene['hgnc_id'])))

def chunk_df(data, nb_row):
    n = round((len(data) / nb_row),1) + 1
    list_df = np.array_split(data, n)
    
    return list_df

def Main(vcf_file,versionVep,dbName,dbUser,dbPwd):

    session = getSession(dbName,dbUser,dbPwd)
    list_GeneTable=[]

    data = []
    i=0

    df_gene = pd.read_sql(session.query(Gene).statement,session.bind)
            
    df_gene['hgnc_id'] = df_gene['hgnc_id'].apply(str)

# =============================================================================
# Lecture du fichier, simplification et split en multiple fichier
# =============================================================================

    print("read files ...")

    
    vcf_reader = vcf.Reader(open(vcf_file,'r'))
    header = vcf_reader.infos["CS"].desc.split(" ")[-1].split("|")

    for record in vcf_reader:


        try:
            infoAllTranscrits_list=record.INFO["CS"]
        
        except: 
        
            continue
        
        var_id = record.ID
            
        for transcrit in infoAllTranscrits_list:
            annotation = transcrit.split('|')
            annotation.insert(0,var_id)
            data.append(annotation)
            i+=1
              
    header.insert(0, 'var_id') 
    df = pd.DataFrame(data, columns=header)
    del_col = ['SYMBOL_SOURCE', 'FLAGS', 'HGNC_ID',"SOMATIC","PHENO",
               "gnomAD_AFR_AF","gnomAD_AMR_AF","gnomAD_ASJ_AF","gnomAD_EAS_AF","gnomAD_FIN_AF",
               "gnomAD_NFE_AF","gnomAD_OTH_AF","gnomAD_SAS_AF","HGVS_OFFSET"]

    df.drop(del_col, axis='columns', inplace=True)

    df = df.fillna('')

    df = df[df['Feature'].str.contains('NM')]
    df.reset_index(inplace=True, drop=True)
   

    print('Mise à jours consequence index')
    
    update = True
    while(update):
        df_consequence = pd.read_sql(session.query(Consequence).statement,session.bind)

        consequence_dict = {}
        
        for row, elt in df_consequence.iterrows():
            consequence_dict[elt['consequence_name']] = elt['consequence_id']
        
        update = mapValueKey(df, consequence_dict, 'Consequence',sessionVarMPK=session)

    print('Export des informations')

    #indexes = np.linspace(0, len(df), num=30, dtype=np.int32)
    df.rename(columns={"MANE_SELECT":"mane_select","MANE_PLUS_CLINICAL":"mane_clinical"},inplace=True)

    
    df_split = np.array_split(df, 2000)
    del(df)
    file_list=[]
    # del(df1)
    for index, df_iterate in enumerate(tqdm(df_split)):
        df1 = df_iterate.copy()
        #print('first  : ' + str(indexes[i]) + '--- > last : ' +  str((indexes[i+1])-1) )
        # df.drop(df.index[indexes[i]:(indexes[i+1])-1], inplace=True)
        # gc.collect()
        
        #  df1[['var_id', 'IMPACT', 'Feature_type','Feature',
        #                          'EXON', 'INTRON', 'CDS_position', 'Consequence',
        #                          'Gene',"HGVSg","HGVSc","HGVSp","gnomAD_AF",
        #                          "mane_select","mane_clinical"]].copy()
        
        
        # df_insert.to_csv(tmp.gettempdir()+'/file_' + str(i) +'.csv', index=False)
        # file_list.append(tmp.gettempdir()+'/file_' + str(i) +'.csv')
        df1["gnomAD_AF"]=df1["gnomAD_AF"].replace("",0)
        df_export = df1[['var_id', 'IMPACT', 'Feature_type','Feature',
                         'EXON', 'INTRON', 'CDS_position', 'Consequence',
                         'Gene',"HGVSg","HGVSc","HGVSp","gnomAD_AF",
                         "mane_select","mane_clinical"]]
        dump(df_export,tmp.gettempdir()+'/file_' + str(index))
        
        file_list.append(tmp.gettempdir()+'/file_' + str(index))
    del(df_split)
    del(df1)
    del(df_export)
    del(vcf_reader)
    session.close()

# =============================================================================
# Debut insertion dans BDD
# =============================================================================

    df_variant_gene = pd.DataFrame(columns=["var_id","hgnc_id"])
    print('Insertion dans la base de données')
    session = getSession(dbName,dbUser,dbPwd)
    for index, file in enumerate(tqdm(file_list)):    

        
        df_i = load(file)
        
        if len(df_i)==0:
            os.remove(file)
            continue
        
        df_i['vep_version'] = versionVep

        df_i.columns = ['var_id','impact','feature_type', 'feature',
                            'exon_pos', 'intron_pos', 'CDS_position', 
                            'consequence_id','hgnc_id', "HGVSg","HGVSc",
                            "HGVSp","gnomAD_AF","mane_select",
                            "mane_clinical",'vep_version']
        
        df_i = df_i.where((pd.notnull(df_i)), None)
        
        ## Verifie la presence du gene dans la bdd
        df_i['hgnc_id'] = df_i['hgnc_id'].apply(str)
        
        gene_absent = checkIfGeneExist(df_i, df_gene)
            
        for elt in gene_absent:
            df_i['hgnc_id'] = df_i['hgnc_id'].replace(elt, '999999999', regex=True)
            
                
        df_i = df_i.replace({np.nan:None})
        
        df_variant_gene=pd.concat([df_variant_gene,df_i.loc[:,['var_id','hgnc_id']]],ignore_index=True)
        df_variant_gene.drop_duplicates(inplace=True)
        
        query = session.execute("SELECT `auto_increment` FROM INFORMATION_SCHEMA.TABLES WHERE table_schema = '" + db_name + "' AND table_name = 'annotations';")


        last_PK_annotation = [{column: value for column, value in rowproxy.items()} for rowproxy in query][0]['auto_increment']-1
        
        session.begin_nested() 
        
        
        session.bulk_insert_mappings(Annotation, df_i.to_dict(orient="records"),return_defaults = True)

        
        session.commit()
        query = session.execute("SELECT `auto_increment` FROM INFORMATION_SCHEMA.TABLES WHERE table_schema = '" + db_name + "' AND table_name = 'annotations';")
        PK_annotation = [{column: value for column, value in rowproxy.items()} for rowproxy in query][0]['auto_increment']-1
        

        if(len(df_i) == (PK_annotation - last_PK_annotation)):
            #print("ok")
            
            df_assoc_var_annot = pd.DataFrame()
            df_assoc_var_annot['annot_id']=range(last_PK_annotation+1, PK_annotation+1)
            # print(df_i['var_id'])
            df_assoc_var_annot['var_id']=df_i['var_id'].to_list()

            session.bulk_insert_mappings(AssociationVariantAnnotation, 
                                            df_assoc_var_annot.to_dict(orient="records"),
                                            return_defaults = True)
                
            df_assoc_var_gene = pd.DataFrame()
            df_assoc_var_gene['hgnc_id']=df_i['hgnc_id']
            df_assoc_var_gene['var_id']=df_i['var_id']
            
            for index_df,row in df_assoc_var_gene.iterrows():
                list_GeneTable.append([row["var_id"],row["hgnc_id"]])
                
                
            session.commit()
            os.remove(file)
            
            
    print("inject var_id hgnc")

    df_assoc_var_geneAll=pd.DataFrame(list_GeneTable,columns=["var_id","hgnc_id"])
    df_assoc_var_geneAll.drop_duplicates(inplace=True,keep='first')
    print(df_assoc_var_geneAll)
    for subdf_assoc_var_gene in chunk_df(df_assoc_var_geneAll,20000):
        session.bulk_insert_mappings(AssociationVariantGene,
                                    subdf_assoc_var_gene.to_dict(orient="records"),return_defaults = True)
             
    session.commit()

    session.close()
    return 

parser = argparse.ArgumentParser(description='', epilog="NAVET Benjamin & GUERACHER Pierre")
parser = argparse.ArgumentParser()

parser.add_argument("-c", "--conf", type=str, help="Conf file with database ID", required=True)
parser.add_argument("-v", "--vcf",type=str,help="Vcf file with all annotation",required=True)
parser.add_argument("-e", "--vep", type=str, help="Specify which VEP you used", required=False,
                    default="VEP106")
args = parser.parse_args()    

if __name__ == '__main__':
    start_time = time.time()
    config = RawConfigParser()

    config.read(args.conf)

    db_name = config.get('DB','name')
    db_user = config.get('DB','user')
    db_pwd = config.get('DB','pwd')
    
    Main(args.vcf,args.vep,db_name,db_user,db_pwd)
    
    print("\nTemps d execution : %s secondes ---" % (time.time() - start_time))