#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Dec 15 10:04:18 2022

@author: louis
This script aims to convert VCF file from mtDNA (Niourk) into proper VCF
that can be injected into the frequency database
"""
import argparse
import time

# Function that updates header of VCF file
# description can be a str or a list of str
def updateHeader(vcf_file,description):
    header = []
    for line in open(vcf_file, 'r'):
        if line.startswith("#"):
            header.append(line.strip())
            
    for elt in header:
        if elt.startswith("##INFO=<ID="):
            i = header.index(elt)
    
    if isinstance(description, str):
        header.insert(i+1, description)
    elif isinstance(description, list):
        for elt in description:
            header.insert(i+1+description.index(elt), elt)
    
    return header
    
# Function that updates variant info field
# field can be a str or a list of str
# Returns an updated var dict
def updateBody(line,field):
    var = getVariant(line,"	")
    if isinstance(field, str):
        i = getFieldIndex(var,field)
        val = getFieldValue(var,i)
        var = updateField(var, "info", field, val)
    elif isinstance(field, list):
        for f in field:
            i = getFieldIndex(var,f)
            val = getFieldValue(var,i)
            var = updateField(var, "info", f, val)
            
    return var
            
# Function that take a variant line from a VCF file
# and returns a dict            
def getVariant(variant_line,sep):
    variant = {"chrom": str(variant_line.strip().split(sep)[0]),
               "pos" : int(variant_line.strip().split(sep)[1]),
               "var_id" : str(variant_line.strip().split(sep)[2]),
               "ref" : str(variant_line.strip().split(sep)[3]),
               "alt" : str(variant_line.strip().split(sep)[4]),
               "qual" : int(variant_line.strip().split(sep)[5]),
               "filter_field" : str(variant_line.strip().split(sep)[6]),
               "info" : str(variant_line.strip().split(sep)[7]),
               "format_field" : str(variant_line.strip().split(sep)[8]),
               "sample" : str(variant_line.strip().split(sep)[9])}
    return variant

# Function that takes a variant dict and a field (as a str)
# The function parse the "format" column of the VCF
# to identify the index of the input field
def getFieldIndex(variant,field):
    i = -1
    is_field = False
    for elt in variant["format_field"].split(':'):
        i += 1
        if elt == field:
            is_field = True
            break
    if is_field == False:
        i = -1
    return i

# Function that takes an index and returns the associated value
# within the sample column
def getFieldValue(variant,index): 
    if index != -1:
        val = variant["sample"].split(":")[index]
    else:
        val = '.'
    return val

# Function that takes the variant dict, the name of the new field
# and the associated value. It returns a variant dict with updated 
# field (in vcf_field/column)
def updateField(variant,vcf_field,field,value):
    buff = variant[vcf_field]
    if buff.endswith(";"):
        buff += str(field)+"="+str(value)
    else:
        buff += ";"+str(field)+"="+str(value)
    variant[vcf_field] = buff
    return variant

# Function that converts variant dict into str
# in order to write it into the output VCF
def variantToLine(variant):
    line = str(variant["chrom"]) + "\t"
    line += str(variant["pos"]) + "\t"
    line += str(variant["var_id"]) + "\t"
    line += str(variant["ref"]) + "\t"
    line += str(variant["alt"]) + "\t"
    line += str(variant["qual"]) + "\t"
    line += str(variant["filter_field"]) + "\t"
    line += str(variant["info"]) + "\t"
    line += str(variant["format_field"]) + "\t"
    line += str(variant["sample"])
    return line

            
if __name__ == '__main__':
    starting_time = time.time()
    parser = argparse.ArgumentParser()
    parser.add_argument("-i", "--input", help="Input VCF file.", type=str, required=True)
    parser.add_argument("-o", "--output", help="Output VCF file (default is same directory as input, with 'modified' suffix)", type=str, default='')
    parser.add_argument('-v', '--version', action='version', version='1.0')
    
    args = parser.parse_args()
    
    infile = args.input
    outfile = args.output
    if outfile == '':
        folder = infile[:-len(infile.split("/")[-1])]
        if infile.endswith(".vcf"):
            outfile = folder + infile.split("/")[-1][:-4] + '_modified.vcf'
            outfile_pass = folder + infile.split("/")[-1][:-4] + '_modified_PASS.vcf' 
        else:
            outfile = folder + infile.split("/")[-1] + '_modified.vcf'
            outfile_pass = folder + infile.split("/")[-1] + '_modified_PASS.vcf'
    else:
        outfile_pass = outfile[:-4] + '_modified_PASS.vcf'


    # Open output VCF file for writing
    out = open(outfile, "w")
    outpass = open(outfile_pass, "w")
    
    # VCF from mito runs are missing 'AF' and 'QD' fields
    # QD is empty as not calculated by strelka nor mutect2
    desc = ['##INFO=<ID=AF,Number=A,Type=Float,Description="Allele Frequency (median if multiple calls)">', '##INFO=<ID=QD,Number=1,Type=Float,Description="Variant Confidence/Quality by Depth">']
    # Update the header
    header = updateHeader(infile, desc)
    # Write header to output VCF file
    for line in header:
        out.write(line + "\n")
        outpass.write(line + "\n")
    
    # Parse VCF file (non-header lines)
    for line in open(infile, 'r'):
        if line.startswith("#") == False:
            # Update variant info
            var = updateBody(line, ["AF","QD"])
            # Convert variant dict to str ('VCF line')
            line = variantToLine(var)
            # Write variant to output VCF file
            out.write(line + "\n")
            if line.strip().split("\t")[6] == "PASS":
                outpass.write(line + "\n")
    
    # Close output VCF file
    out.close()