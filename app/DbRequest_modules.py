
import re
from datetime import date, datetime
import logging
import pandas as pd
from sqlalchemy import and_
from DB_class import (Annotation,  # CustomAnnotation,
                      AssociationVariantAnnotation, AssociationVariantSample,
                      Consequence, Experiment, Gene, Sample, Variant)
from DB_connection import getSession
from DB_method import getFrequencies, getMANETranscrits

from DB_class import (Annotation, AssociationVariantAnnotation,
                      AssociationVariantGene, Gene, Variant)
from operator import itemgetter
import numpy as np
class DbRequest():
    
    __search = ""
    __typeSearch = ""
    __freqGnomAd_min = 0.0
    __freqGnomAd_max = 1.0
    __freqCHU_min = 0.0
    __freqCHU_max = 1.0
    __sampleList = []
    __filterSample = False
    __period = f"1800-01-01/{date.today()}"
    __oldest_date = datetime.strptime('1800-01-01','%Y-%m-%d').date()
    __newest_date = date.today()
    __filterPeriod = False
    __vafHtz_min = 0.0
    __vafHtz_max = 1.0
    __vafHom_min = 0.0
    __vafHom_max = 1.0
    __vafLow_min = 0.0
    __vafLow_max = 1.0
    __occTot_min = 0 
    __occHtz_min = 0
    __occHom_min = 0
    __occLow_min = 0
    __occTot_max = -1 
    __occHtz_max = -1
    __occHom_max = -1
    __occLow_max = -1
    __consequence = []
    __filterConsequence = False
    __impact = []
    __filterImpact = False
    __exportCSV = False
    __passFilter = True
    __message = ""

    def __init__(self,):
        
        self.__search = DbRequest.__search
        self.__typeSearch = DbRequest.__typeSearch
        self.__freqGnomAd_min = DbRequest.__freqGnomAd_min
        self.__freqCHU_min = DbRequest.__freqCHU_min
        self.__freqCHU_max = DbRequest.__freqCHU_max
        self.__sampleList = DbRequest.__sampleList
        self.__filterSample = DbRequest.__filterSample
        self.__period = DbRequest.__period
        self.__oldest_date = DbRequest.__oldest_date
        self.__newest_date = DbRequest.__newest_date
        self.__filterPeriod = DbRequest.__filterPeriod
        self.__vafHtz_min = DbRequest.__vafHtz_min
        self.__vafHtz_max = DbRequest.__vafHtz_max
        self.__vafHom_min = DbRequest.__vafHom_min
        self.__vafHom_max = DbRequest.__vafHom_max
        self.__vafLow_min = DbRequest.__vafLow_min
        self.__vafLow_max = DbRequest.__vafLow_max
        self.__occTot_min = DbRequest.__occTot_min
        self.__occHtz_min = DbRequest.__occHtz_min
        self.__occHom_min = DbRequest.__occHom_min
        self.__occLow_min = DbRequest.__occLow_min
        self.__occTot_max = DbRequest.__occTot_max
        self.__occHtz_max = DbRequest.__occHtz_max
        self.__occHom_max = DbRequest.__occHom_max
        self.__occLow_max = DbRequest.__occLow_max
        self.__consequence = DbRequest.__consequence
        self.__filterConsequence = DbRequest.__filterConsequence
        self.__impact = DbRequest.__impact
        self.__filterImpact = DbRequest.__filterImpact
        self.__exportCSV = DbRequest.__exportCSV
        self.__passFilter = DbRequest.__passFilter
        self.__message = DbRequest.__message
        

        
    def __repr__(self):
        return f"Request_{self.search}"
        
    @classmethod
    def instantiate_from_excel(cls,path,exportRequest):
        list_query = [] 
        df_request = pd.read_excel(path,skiprows=13)
        
        df_request.fillna("",inplace=True)
        
        for index, row in df_request.iterrows():
            listImpact=[]
            listConsequence=[]
            
            if row["recherche"] == "":
                break
            
            if row["impacts"] != "":
                listImpact = row["impacts"].split("/")
                
            if row["consequences"] != "":
                listConsequence = row["consequences"].split("/")
                
            user_query = cls()
            
            user_query.update_criteria(search = row["recherche"],
            freqgnomad_min=row["freq_GnomAd_min"],
            freqgnomad_max=row["freq_GnomAd_max"],
            freqCHU_min=row["freq_CHU_min"],freqCHU_max=row["freq_CHU_max"],
            samplseStr=row["echantillons"],period=row["periode"],
            vaf_htz_min=row["freq_htz_min"], vaf_htz_max=row["freq_htz_max"],
            vaf_hom_min=row["freq_hom_min"],vaf_hom_max=row["freq_hom_max"],
            vaf_lowest_min=row["freq_low_min"],vaf_lowest_max=row["freq_low_max"],
            occ_tot_min=row["occ_totale_min"],occ_htz_min=row["occ_htz_min"],
            occ_hom_min=row["occ_hom_min"], occ_low_min=row["occ_low_min"],
            occ_tot_max=row["occ_totale_max"],occ_htz_max=row["occ_htz_max"],
            occ_hom_max=row["occ_hom_max"], occ_low_max=row["occ_low_max"],
            consequence=listConsequence,
            impact=listImpact,exportCSV=exportRequest)
                
            list_query.append(user_query)
        return list_query
    @property
    def search(self):
        return self.__search
    
    @property
    def typeSearch(self):
        return self.__typeSearch
    
    @property
    def freqGnomAd_min(self):
        return self.__freqGnomAd_min
    
    @property
    def freqGnomAd_max(self):
        return self.__freqGnomAd_max
    
    @property
    def freqCHU_max(self):
        return self.__freqCHU_max
    @property
    def freqCHU_min(self):
        return self.__freqCHU_min
    
    @property
    def filterSample(self):
        return self.__filterSample
    
    @property
    def sampleList(self):
        return self.__sampleList
    
    @property
    def filterPeriod(self):
        return self.__filterPeriod
    
    @property
    def period(self):
        return self.__period
    
    @property
    def oldest_date(self):
        return self.__oldest_date
    
    @property
    def newest_date(self):
        return self.__newest_date
    
    @property
    def vafHtz_min(self):
        return self.__vafHtz_min
    
    @property
    def vafHtz_max(self):
        return self.__vafHtz_max
    
    @property
    def vafHom_min(self):
        return self.__vafHom_min
    
    @property
    def vafHom_max(self):
        return self.__vafHom_max
    
    @property
    def vafLow_min(self):
        return self.__vafLow_min
    
    @property
    def vafLow_max(self):
        return self.__vafLow_max
    
    @property
    def occTot_min(self):
        return self.__occTot_min
    
    @property
    def occHtz_min(self):
        return self.__occHtz_min
    
    @property
    def occHom_min(self):
        return self.__occHom_min
    
    @property
    def occLow_min(self):
        return self.__occLow_min
    
    @property
    def occTot_max(self):
        return self.__occTot_max
    
    @property
    def occHtz_max(self):
        return self.__occHtz_max
    
    @property
    def occHom_max(self):
        return self.__occHom_max
    
    @property
    def occLow_max(self):
        return self.__occLow_max
    
    
    @property
    def consequence(self):
        return self.__consequence
    
    @property
    def filterConsequence(self):
        return self.__filterConsequence
    
    @property
    def impact(self):
        return self.__impact
    
    @property
    def filterImpact(self):
        return self.__filterImpact

    
    @property
    def exportCSV(self):
        return self.__exportCSV
    
    @property
    def passFilter(self):
        return self.__passFilter

    @property
    def message(self):
        return self.__message
    
    def update_criteria(self,search,freqgnomad_min,freqgnomad_max,
                 freqCHU_min,freqCHU_max,samplseStr,period,
                 vaf_htz_min, vaf_htz_max,vaf_hom_min,vaf_hom_max
                 ,vaf_lowest_min,vaf_lowest_max,occ_tot_min,
                 occ_htz_min,occ_hom_min,occ_low_min,occ_tot_max,
                 occ_htz_max,occ_hom_max,occ_low_max,consequence,impact,exportCSV):
        
        self.search = search
        self.freqGnomAd_min = freqgnomad_min
        self.freqGnomAd_max = freqgnomad_max
        self.freqCHU_min = freqCHU_min
        self.freqCHU_max = freqCHU_max
        self.sampleList = samplseStr
        self.period = period
        self.vafHtz_min = vaf_htz_min
        self.vafHtz_max =  vaf_htz_max
        self.vafHom_min = vaf_hom_min
        self.vafHom_max = vaf_hom_max
        self.vafLow_min = vaf_lowest_min
        self.vafLow_max = vaf_lowest_max
        self.occTot_min = occ_tot_min
        self.occHtz_min = occ_htz_min
        self.occHom_min = occ_hom_min
        self.occLow_min = occ_low_min
        self.occTot_max = occ_tot_max
        self.occHtz_max = occ_htz_max
        self.occHom_max = occ_hom_max
        self.occLow_max = occ_low_max
        self.consequence = consequence
        self.impact = impact
        self.exportCSV = exportCSV
        
    
    def getTypeSearch(self,value):

        dicoRegex = {"HGVSp":"^[NX]P_[0-9]*.[0-9]*:p[.]",
                "HGVSc":"^NM_[0-9]*.[0-9]*:c[.]",
                "HGVSg":"^[0-9 XYM]:g[.]",
                'feature':"^NM_[0-9]*$",
                'coord_tiret':"^[0-9]*[-][0-9]*[-][ATCG]*[-][ATCG]*",
                "hgnc_name":"^[A-Z0-9]{3,10}"
                }

        for type_search in dicoRegex.keys():
            check=bool(re.match(dicoRegex[type_search],value))
            
            if check:
                return type_search
        return False
    
    def getRealConsequence(self,listConsequence):
        
        list_consequence = []
        
        consequence_dict = {"Missense":'missense_variant',"Intronic":'intron_variant',"Stop_gain":'stop_gained',
                            "Synonym":'synonymous_variant', "Start_lost":'start_lost',"Frameshift":'frameshift_variant',
                            "Splice":'splice_variant', "Protein_altering":'protein_altering_variant',
                            "Inframe":'inframe_variant',
                            "Others":'others'
        }
        
        for csq in listConsequence:
            
            list_consequence.append(consequence_dict[csq])
            
        if(len(list_consequence) !=0):
            if('splice_variant' in list_consequence):
                list_consequence.remove('splice_variant')
                list_consequence.extend(('splice_acceptor_variant', 
                                        'splice_donor_variant'))
            if('inframe_variant' in list_consequence):
                list_consequence.remove('inframe_variant')
                list_consequence.extend(('inframe_deletion', 'inframe_insertion'))
            if('others' in list_consequence):
                list_consequence.remove('others')
                list_consequence.extend(('intergenic_variant', 'downstream_gene_variant',
                                        'upstream_gene_variant', '5_prime_UTR_variant',
                                        '3_prime_UTR_variant'))
                
        return list_consequence

    @typeSearch.setter
    def typeSearch(self,value):
        self.__typeSearch = value

    @filterImpact.setter
    def filterImpact(self,value):
        self.__filterImpact = value
        
    @filterConsequence.setter
    def filterConsequence(self,value):
        self.__filterConsequence = value
        
    @filterSample.setter
    def filterSample(self,value):
        self.__filterSample = value
        
    @filterPeriod.setter
    def filterPeriod(self,value):
        self.__filterPeriod = value
        
    @exportCSV.setter
    def exportCSV(self,value):
        self.__exportCSV = value
        
    @passFilter.setter
    def passFilter(self,value):
        self.__passFilter = value
        
    @message.setter
    def message(self,value):
        self.__message = value
    
    @search.setter
    def search(self,value_enter):
        value  = value_enter.strip() 
        
        logging.warning(value)     
        typeSearch = self.getTypeSearch(value)

        if typeSearch == False:
        
            raise Exception(f"""Le type de votre requête {value} n'est pas 
                            reconnue \n Veuillez contacter l'administateur""")
        else:
            self.__typeSearch = typeSearch
            self.__search = value
            
    @freqGnomAd_min.setter
    def freqGnomAd_min(self,value):
        if value !="":
            try:
                self.__freqGnomAd_min = float(value.replace(',', '.'))
            except ValueError:
                raise ValueError('Not a valid float value')
            except AttributeError:
                self.__freqGnomAd_min = float(value)
        
            if (self.freqGnomAd_min < 0) or (self.freqGnomAd_min > 1):
                raise Exception("""Les fréquences doivent être entre 0 et 1""")
    
    @freqGnomAd_max.setter
    def freqGnomAd_max(self,value):
        
        if value !="":
            try:
                self.__freqGnomAd_max = float(value.replace(',', '.'))
            except ValueError:
                raise ValueError('Not a valid float value')
            except AttributeError:
                self.__freqGnomAd_max = float(value)
        
            if (self.__freqGnomAd_max < 0) or (self.__freqGnomAd_max > 1):
                raise Exception("""Les fréquences doivent être entre 0 et 1""")
            

        
    @freqCHU_min.setter
    def freqCHU_min(self,value):
        if value !="":
            try:
                self.__freqCHU_min = float(value.replace(',', '.'))
            except ValueError:
                raise ValueError('Not a valid float value')
            except AttributeError:
                self.__freqCHU_min = float(value)
            
            if (self.__freqCHU_min < 0) or (self.__freqCHU_min > 1):
                raise Exception("""Les fréquences doivent être entre 0 et 1""")
            

        
    @freqCHU_max.setter
    def freqCHU_max(self,value):
        if value !="":
            try:
                self.__freqCHU_max = float(value.replace(',', '.'))
            except ValueError:
                raise ValueError("Veuillez mettre un chiffre à virgule")
            except AttributeError:
                self.__freqCHU_max = float(value)
            
            if (self.__freqCHU_max < 0) or (self.__freqCHU_max > 1):
                raise Exception("""Les fréquences doivent être entre 0 et 1""")
            

            
    @sampleList.setter
    def sampleList(self,value):
        
        if value!="":
            listSample = value.split("/")
            
            self.__sampleList = listSample
            self.filterSample = True
            
    @period.setter
    def period(self,value):
        
        if value != "":
            
            dates_list = value.split("/")
            if len(dates_list) != 2:
                raise Exception(f"""Période incorrecte : {value}
                                [Defaut: {self.oldest_date}/{self.newest_date}""")
            try:
                self.__oldest_date = datetime.strptime(dates_list[0],'%Y-%m-%d').date()
                self.__newest_date = datetime.strptime(dates_list[1],'%Y-%m-%d').date()
                self.__filterPeriod = True
            except: 
                raise Exception("Format de date incorrect. AAAA-MM-JJ")
            
            if self.__newest_date < self.__oldest_date:
                raise Exception("Les dates sont inversées")
            
    @vafHtz_min.setter
    def vafHtz_min(self,value):
        if value !="":
            try:
                self.__vafHtz_min = float(value.replace(',', '.'))
            except ValueError:
                raise ValueError("Veuillez mettre un chiffre à virgule")
            except AttributeError:
                self.__vafHtz_min = float(value)
            if (self.__vafHtz_min < 0) or (self.__vafHtz_min > 1):
                raise Exception("""Les fréquences doivent être entre 0 et 1""")

            
    @vafHtz_max.setter
    def vafHtz_max(self,value):
        if value !="":
            try:
                self.__vafHtz_max = float(value.replace(',', '.'))
            except ValueError:
                raise ValueError("Veuillez mettre un chiffre à virgule")
            except AttributeError:
                self.__vafHtz_max = float(value)
            if (self.__vafHtz_max < 0) or (self.__vafHtz_max > 1):
                raise Exception("""Les fréquences doivent être entre 0 et 1""")

            
    @vafHom_min.setter
    def vafHom_min(self,value):
        if value !="":
            try:
                self.__vafHom_min = float(value.replace(',', '.'))
            except ValueError:
                raise ValueError("Veuillez mettre un chiffre à virgule")
            except AttributeError:
                self.__vafHom_min = float(value)
            if (self.__vafHom_min < 0) or (self.__vafHom_min > 1):
                raise Exception("""Les fréquences doivent être entre 0 et 1""")

            
    @vafHom_max.setter
    def vafHom_max(self,value):
        if value !="":
            try:
                self.__vafHom_max = float(value.replace(',', '.'))
            except ValueError:
                raise ValueError("Veuillez mettre un chiffre à virgule")
            except AttributeError:
                self.__vafHom_max = float(value)
            if (self.__vafHom_max < 0) or (self.__vafHom_max > 1):
                raise Exception("""Les fréquences doivent être entre 0 et 1""")

            
    @vafLow_min.setter
    def vafLow_min(self,value):
        if value !="":
            try:
                self.__vafLow_min = float(value.replace(',', '.'))
            except ValueError:
                raise ValueError("Veuillez mettre un chiffre à virgule")
            except AttributeError:
                self.__vafLow_min = float(value)
            if (self.__vafLow_min < 0) or (self.__vafLow_min > 1):
                raise Exception("""Les fréquences doivent être entre 0 et 1""")

            
    @vafLow_max.setter
    def vafLow_max(self,value):
        if value !="":
            try:
                self.__vafLow_max = float(value.replace(',', '.'))
            except ValueError:
                raise ValueError("Veuillez mettre un chiffre à virgule")
            except AttributeError:
                self.__vafLow_max = float(value)
            if (self.__vafLow_max < 0) or (self.__vafLow_max > 1):
                raise Exception("""Les fréquences doivent être entre 0 et 1""")

            
    @occTot_min.setter
    def occTot_min(self,value):
        
        if value !="":
            
            try: self.__occTot_min = int(value)
            except ValueError:
                raise ValueError("Veuillez mettre un nombre entier")
            
            if self.__occTot_min < 0 :
                raise Exception("""Les occurences ne peuvent pas être négatives""")
    
    @occHom_min.setter
    def occHom_min(self,value):
        
        if value !="":
            
            try: self.__occHom_min = int(value)
            except ValueError:
                raise ValueError("Veuillez mettre un nombre entier")
            
            if self.__occHom_min < 0 :
                raise Exception("""Les occurences ne peuvent pas être négatives""")
            
    @occHtz_min.setter
    def occHtz_min(self,value):
        
        if value !="":
            
            try: self.__occHtz_min = int(value)
            except ValueError:
                raise ValueError("Veuillez mettre un nombre entier")
            
            if self.__occHtz_min < 0 :
                raise Exception("""Les occurences ne peuvent pas être négatives""")
            
    @occLow_min.setter
    def occLow_min(self,value):
        
        if value !="":
            
            try: self.__occLow_min = int(value)
            except ValueError:
                raise ValueError("Veuillez mettre un nombre entier")
            
            if self.__occLow_min < 0 :
                raise Exception("""Les occurences ne peuvent pas être négatives""")
            
    @occTot_max.setter
    def occTot_max(self,value):
        
        if value !="":
            
            try: self.__occTot_max = int(value)
            except ValueError:
                raise ValueError("Veuillez mettre un nombre entier")
            
            if self.__occTot_max < 0 :
                raise Exception("""Les occurences ne peuvent pas être négatives""")
            
    @occHtz_max.setter
    def occHtz_max(self,value):
        
        if value !="":
            
            try: self.__occHtz_max = int(value)
            except ValueError:
                raise ValueError("Veuillez mettre un nombre entier")
            
            if self.__occHtz_max < 0 :
                raise Exception("""Les occurences ne peuvent pas être négatives""")
            
    @occHom_max.setter
    def occHom_max(self,value):
        
        if value !="":
            
            try: self.__occHom_max = int(value)
            except ValueError:
                raise ValueError("Veuillez mettre un nombre entier")
            
            if self.__occHom_max < 0 :
                raise Exception("""Les occurences ne peuvent pas être négatives""")

            
    @occLow_max.setter
    def occLow_max(self,value):
        
        if value !="":
            
            try: self.__occLow_max = int(value)
            except ValueError:
                raise ValueError("Veuillez mettre un nombre entier")
            
            if self.__occLow_max < 0 :
                raise Exception("""Les occurences ne peuvent pas être négatives""")
            
    @consequence.setter
    def consequence(self,value):
        
        if isinstance(value,str):
            listCsq = value.split(";")
        else:
            listCsq = value
        
        if len(listCsq) != 0:
            
            self.__consequence = self.getRealConsequence(listCsq)
            self.__filterConsequence = True
            

            
    @impact.setter
    def impact(self,value):
        
        if isinstance(value,str):
            valueUpper = value.upper()
            listImp = valueUpper.split(";")
        else:
            listImp = value
        
        if len(listImp) != 0:
            
            self.__impact = listImp
            self.__filterImpact = True

        
        
    def set_occ_max(self,sample_number):
        
        if self.occTot_max == -1:
           self.__occTot_max = sample_number 
        if self.occHtz_max == -1:
            self.__occHtz_max = sample_number
            
        if self.occHom_max == -1:
            self.__occHom_max = sample_number
            
        if self.occLow_max == -1:
            self.__occLow_max = sample_number
        
    def requestVarId(queryObj,dbName,dbUser,dbPwd):
        
        #---------------------------------|
        #         REQUETE SUR DB          |
        #---------------------------------|
        session_sql = getSession(dbName,dbUser,dbPwd)
        
        if queryObj.typeSearch == "hgnc_name":

            if (queryObj.search.isdigit()==False):
                subquery_hgnc_id = (session_sql.query(Gene).filter(Gene.hgnc_name.like(queryObj.search)))
                df_hgnc_id = pd.read_sql(subquery_hgnc_id.statement,session_sql.bind)
                search_list = df_hgnc_id.hgnc_id.to_list()
                
                req_var_id = (session_sql.query(Variant.var_id)
        .join(AssociationVariantGene, AssociationVariantGene.var_id == Variant.var_id)
        .join(Gene, Gene.hgnc_id ==  AssociationVariantGene.hgnc_id).distinct()
        .filter(Gene.hgnc_id.in_(search_list)))
                
            else:
                
                req_var_id = (session_sql.query(Variant.var_id)
        .join(AssociationVariantGene, AssociationVariantGene.var_id == Variant.var_id)
        .join(Gene, Gene.hgnc_id ==  AssociationVariantGene.hgnc_id).distinct()
        .filter(Gene.hgnc_id.in_([queryObj.search])))

        elif queryObj.typeSearch == "coord_tiret":
            chrom,pos,ref,alt = queryObj.search.split("-")

            req_var_id = (session_sql.query(Variant.var_id).filter(and_(Variant.chrom == f"chr{chrom}",
                Variant.pos == pos,Variant.ref == ref,
                Variant.alt == alt)))

        elif queryObj.typeSearch == "feature":
            req_var_id = (session_sql.query(Variant.var_id)
        .join(AssociationVariantAnnotation, AssociationVariantAnnotation.var_id == Variant.var_id)
        .join(Annotation, Annotation.annot_id == AssociationVariantAnnotation.annot_id)
        .filter(Annotation.feature.contains(queryObj.search)))
            
        elif queryObj.typeSearch == "HGVSg":
            req_var_id = (session_sql.query(Variant.var_id)
        .join(AssociationVariantAnnotation, AssociationVariantAnnotation.var_id == Variant.var_id)
        .join(Annotation, Annotation.annot_id == AssociationVariantAnnotation.annot_id)
        .filter(Annotation.HGVSg == queryObj.search))
            
        elif queryObj.typeSearch == "HGVSc":
            req_var_id = (session_sql.query(Variant.var_id)
        .join(AssociationVariantAnnotation, AssociationVariantAnnotation.var_id == Variant.var_id)
        .join(Annotation, Annotation.annot_id == AssociationVariantAnnotation.annot_id)
        .filter(Annotation.HGVSc == queryObj.search))
                
        elif queryObj.typeSearch == "HGVSp":
            req_var_id = (session_sql.query(Variant.var_id)
        .join(AssociationVariantAnnotation, AssociationVariantAnnotation.var_id == Variant.var_id)
        .join(Annotation, Annotation.annot_id == AssociationVariantAnnotation.annot_id)
        .filter(Annotation.HGVSp == queryObj.search))
            
        else:
            req_var_id = False
            
        return  req_var_id
            
    def makeQuery(self,dbName,dbUser,dbPwd):
        session_sql = getSession(dbName,dbUser,dbPwd)
        nbr_sample = session_sql.query(Sample).count()
        
        req_var_id =  self.requestVarId(dbName,dbUser,dbPwd)


        filtered_variant = pd.read_sql(req_var_id.statement,session_sql.bind)["var_id"].to_list()
        
        
        if len(filtered_variant)==0:
            self.passFilter = False
            self.message = f"""Requete {self.search}:  Absent de la base de données."""

        else:
            self.var_id = filtered_variant
            
            requestDB = (session_sql.query(Gene.hgnc_name,Variant.var_id,Annotation.feature, Annotation.HGVSg,
                                        Annotation.HGVSc,Annotation.HGVSp,Sample.sample_number,
                                        AssociationVariantSample.af_gatk,Annotation.mane_select,
                                        Annotation.gnomAD_AF,Annotation.annot_id,Consequence.consequence_name
                                        ,Annotation.impact,Experiment.run_number,Experiment.run_date,
                                        Experiment.run_name, Annotation.exon_pos,Annotation.intron_pos)
            .join(AssociationVariantSample, AssociationVariantSample.var_id == Variant.var_id)
            .join(Sample, AssociationVariantSample.sample_id == Sample.sample_id)
            .join(Experiment, Sample.run_id == Experiment.run_id)
            .join(AssociationVariantAnnotation, AssociationVariantAnnotation.var_id == Variant.var_id)
            .join(Annotation, Annotation.annot_id == AssociationVariantAnnotation.annot_id)
            .join(Gene, Gene.hgnc_id == Annotation.hgnc_id)
            .join(Consequence, Consequence.consequence_id == Annotation.consequence_id)
            .filter(Variant.var_id.in_(self.var_id)))
        
            data_request = pd.read_sql(requestDB.statement,session_sql.bind)
            
            if self.typeSearch == "hgnc_name":

                data_request = data_request.loc[data_request["hgnc_name"]==self.search,:]

            
            
            data_request = getMANETranscrits(data_request)

            data_request["gnomAD_AF"] = data_request["gnomAD_AF"].fillna(value="0")
        
            ###Attention avec cette commande. Modifie les calcul de fréquences en fonction des 
            # concaténations - Toujours vérifier les sorties web avec les commandes mysql.
            

            
            data_request = data_request.groupby(["hgnc_name","var_id",
                                            "HGVSg","HGVSc","HGVSp",
                                            "annot_id","feature",
                                            "gnomAD_AF","consequence_name",
                                            'impact',"intron_pos","exon_pos"], dropna=False
                                            ,as_index=False).agg(lambda x: list(x.to_list()))
            
           
        
            data_request = data_request.fillna(value="-")
            self.brut_results = getFrequencies(data_request,nbr_sample)
            
            
    def applyFilter(self,nbrSample):
        
        if self.passFilter == True:
            try: 
                if self.typeSearch=="feature":
                    data_filtered=self.brut_results.loc[self.brut_results[self.typeSearch].str.contains(self.search),:]
                else:
                    data_filtered=self.brut_results.loc[self.brut_results[self.typeSearch]==self.search,:]
                    
            except:
                data_filtered=self.brut_results
            
            self.set_occ_max(nbrSample)

            
            
            data_filtered.query(f"{self.freqGnomAd_min} <= gnomAD_AF <= {self.freqGnomAd_max}\
                            & {self.freqCHU_min} <= total_freq <= {self.freqCHU_max}\
                            & {self.vafHtz_min} <= htz_freq <= {self.vafHtz_max}\
                            & {self.vafHom_min} <= hom_freq <= {self.vafHom_max}\
                            & {self.vafLow_min} <= low_freq <= {self.vafLow_max}\
                            & {self.occTot_min} <= total_count <= {self.occTot_max}\
                            & {self.occHtz_min} <= htz_count <= {self.occHtz_max}\
                            & {self.occHom_min} <= hom_count <= {self.occHom_max}\
                            & {self.occLow_min} <= low_count <= {self.occLow_max}",inplace=True)
            
            if len(data_filtered)==0:
                
                self.message = f"""Requete {self.search} : Les filtres renseignés n'ont pas permis d'obtenir des résultats.\n
                                    Dernier filtre utilisé : Fréquences et occurences"""

                self.passFilter = False
                    
    
            if self.filterConsequence and self.passFilter:

                print("Filtre on consequences")
                data_filtered=data_filtered.loc[data_filtered["consequence_name"].isin(self.consequence),:]
                
                if len(data_filtered)==0:
                    
                    self.message = f"""Requete {self.search}: Les filtres renseignés n'ont pas permis d'obtenir des résultats.\n
                                    Dernier filtre utilisé : Conséquence {self.consequence}"""
                                    
                    self.passFilter = False


            if self.filterImpact and self.passFilter:
                
                print("Filtre on impact")
                data_filtered=data_filtered.loc[data_filtered["impact"].isin(self.impact),:]
                if len(data_filtered)==0:
                    self.passFilter = False
                    self.message = f"""Requete {self.search}: Les filtres renseignés n'ont pas permis d'obtenir des résultats.\n
                                    Dernier filtre utilisé : Impact {self.impact}"""


            #Pour les requetes suivantes, les valeurs sont dans des listes. Il est pas possible de filtrer directement.
            
            #Il faut rentrer dans la liste, verifier que la condition est verifie pour chaque variant. 
            
            #Si ce n est pas le cas, la ligne est supprime


            data_filtered.drop_duplicates(subset=['var_id'],inplace=True)


            if self.filterSample:
                row_delete=0
                row_number=len(data_filtered)
                
                for index,row in data_filtered.iterrows():
                    list_SampleIndexPresent = []
                    check_samplePresent=False
                    
                    for sample in self.sampleList:

                        if sample in row["sample_number"]:

                            index_sample = [i for i, x in enumerate(row["sample_number"]) if x == sample]
                            
                            
                            check_samplePresent=True
                            list_SampleIndexPresent.extend(index_sample)
                            
                        
                    if check_samplePresent==False: 
                        
                        data_filtered.drop(index,inplace=True)
                        row_delete+=1
                    else:

                        data_filtered.at[index,"sample_number"] = [row["sample_number"][index_var] for index_var in list_SampleIndexPresent]
                        data_filtered.at[index,"run_date"] = [row["run_date"][index_var] for index_var in list_SampleIndexPresent]
                        data_filtered.at[index,"run_name"] = [row["run_name"][index_var] for index_var in list_SampleIndexPresent]

                if row_delete == row_number:
                    
                    self.message = f"""Requete {self.search}: Les filtres renseignés n'ont pas permis d'obtenir des résultats.\n
                                        Dernier filtre utilisé : ID Echantillon {self.sampleList}"""
                    self.passFilter = False

            if self.filterPeriod:
                row_delete=0
                row_number=len(data_filtered)
                for index,row in data_filtered.iterrows():
                    list_PeriodIndexPresent = []
                    in_period = False
                    
                    for index_period,date_seq in enumerate(row["run_date"]):
                        
                        if ((date_seq >= self.oldest_date) and
                            (date_seq <= self.newest_date)):
                            
                            list_PeriodIndexPresent.append(index_period)
                            in_period =True
                                     
                    if in_period == False:
                        data_filtered.drop(index,inplace=True)
                        row_delete+=1
                        
                    else:
                        
                        data_filtered.at[index,"sample_number"]= [row["sample_number"][index_var] for index_var in list_PeriodIndexPresent]
                        data_filtered.at[index,"run_date"]= [row["run_date"][index_var] for index_var in list_PeriodIndexPresent]
                        data_filtered.at[index,"run_name"]= [row["run_name"][index_var] for index_var in list_PeriodIndexPresent]
                if row_delete == row_number: 
                    
                    self.message = f"""Requete {self.search}: Les filtres renseignés n'ont pas permis d'obtenir des résultats.\n
                                    Dernier filtre utilisé : Periode {self.oldest_date}/{self.newest_date}"""
                    self.passFilter = False

            if self.passFilter:
                
                    self.df_filtered = data_filtered
                    
                    

    
            
                    

                    

        
    


if (__name__  == '__main__'):
    pass
    # DbRequest(search ="SAMD11",
    #     freqgnomad_min="",
    #     freqgnomad_max="",
    #     freqCHU_min="",freqCHU_max="",
    #     samplseStr="",period="",
    #     vaf_htz_min="", vaf_htz_max="",
    #     vaf_hom_min="",vaf_hom_max="",
    #     vaf_lowest_min="",vaf_lowest_max="",
    #     occ_tot="",occ_htz="",occ_hom="",
    #     occ_low="",consequence=[],
    #     impact=[],formQuery=True)
    
    # for query in DbRequest.all :
        
    #     query.makeQuery("","","")

    # for query in DbRequest.all :
    #     query.applyFilter()
    
    
    # list_val=DbRequest.instantiate_from_excel("./templates/storage_public/modele_requete_par_lot.xlsx",False)
    
    # for val in list_val:
        
    #     print(val.__dict__)
    

