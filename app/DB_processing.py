#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import glob
from multiprocessing import Pool
import numpy as np
from multiprocessing import Manager
import pandas as pd
import time
import subprocess
import sys
import os
import gc
import argparse
from sqlalchemy.orm.exc import NoResultFound
from sqlalchemy.orm.exc import MultipleResultsFound
from functools import partial
from sqlalchemy.exc import InvalidRequestError
from datetime import datetime
import re
from configparser import RawConfigParser
start_time = time.time()
import global_

from DB_connection import getSession
from DB_class import Base, Experiment, Sequenceur, Sample, Variant, AssociationVariantSample, Gene, Call#, AssociationVariantGene
from DB_method import zygotieStatus, checkVariant, getAFmedian, VEP_csq_traitment, calculAF_corr, checkIfGeneExist, splitMultiCaller, getCallerCol

   
def _get_date():
    return datetime.datetime.now()


def getSpecification(csvFile):
    
    tab = pd.read_csv(csvFile,sep="\t")
    listAll=[]
    print(f"Tab:{tab}")
    for val in tab.iterrows():
        listeRow=[]
        
        listeRow.append(f'20{val[1]["FOLDER_NAME"].rstrip().split("_")[0]}')
        listeRow.append(val[1]["NAME"])
        
        listAll.append(listeRow)
        
    return listAll

def splitCallTable(col_pos, caller_list, df,s_idVar):
    df_tmp = pd.DataFrame(columns=['call_id','var_id','sample_id','pipeline_id','caller_name','af','qual'])
        
    for index, row in df.iterrows():
        i = 0
        for caller in caller_list:
           
            new_call = {}
            new_call['call_id'] = None
            new_call['var_id'] = row['var_id']
            new_call['sample_id'] = s_idVar
            new_call['pipeline_id'] = 1
            new_call['caller_name'] = caller.lower()
            new_call['af'] = row[col_pos[i][0]]
            new_call['qual'] = row[col_pos[i][1]]
            if(new_call['af'] != '.'):
               df_tmp = df_tmp.append(new_call, ignore_index=True) 
            
            i+=1
        
    return df_tmp

def chunk_df(data, nb_row):
    n = round((len(data) / nb_row),1) + 1
    list_df = np.array_split(data, n)

    return list_df

def db_processing(path_vcfSimplify,vcf_file,confFile,expName,
                  runType,runDate,sequenceurId,threadVar):
    
    config = RawConfigParser()

    config.read(confFile)
    global_.db_name = config.get('DB','name')
    global_.db_user = config.get('DB','user')
    global_.db_pwd = config.get('DB','pwd')

    db_name = config.get('DB','name')
    db_user = config.get('DB','user')
    db_pwd = config.get('DB','pwd')
    try: os.path.exists(path_vcfSimplify)
    except:
        print("L'executable VcfSimplify n'existe pas")
        sys.exit(1)
        
        
    session = getSession(db_name,db_user,db_user)
    
    # =============================================================================
    # Ouverture du fichier de variant
    # =============================================================================

    vcf_filename=os.path.splitext(os.path.basename(vcf_file))[0]

    #Split file name for extract sample and run name

    name = vcf_filename.split('_')[0]
    if(name == "BLC"):
        sys.exit("BLANC")

    ##Cas des anciens fichiers integragen
    if(name == "INT"):
        sample_name = vcf_filename.split('_vep')[0]
        sample_name = sample_name[4:]
    else:
        sample_name = vcf_filename.split('_')[1]

    #Query database for get run_id if exists

    r_id=None

    try : 
        result_run = session.query(Experiment).filter_by(run_name = expName).one()
        session.commit()
        run_name = expName
        r_id=result_run.run_id
    except MultipleResultsFound : 
        print("Le run est présent plusieurs fois")
    except NoResultFound :
        print("Ajout d'un nouveau run...")
        run_name = expName
        e1 = Experiment(run_name=expName, run_type=runType, run_date = runDate, sequenceur_id = sequenceurId) 
        session.add(e1)
        session.flush()
        r_id = e1.run_id
        
    
    print("RUN ID IDENTIFIE : " + str(r_id))
        
    # #Query database for identify if sample is already present

    result_sample = session.query(Sample).filter(Sample.sample_number==sample_name)
    print("Identifiant de run :" + str(r_id) + "\n")

    if(result_sample.count() == 0):
        print("Ajout d'un sample : \n")
        s1 = Sample(sample_number= sample_name, run_id=r_id)
        session.add(s1)
        session.flush()
        s_id = s1.sample_id
    elif(result_sample.count() == 1 ):
        old_sample = result_sample.one()
        session.commit()
        print("[ERROR] : L'échantillon est déjà présent sous l'id " + str(old_sample.sample_id)+"\n\n")
        #print('FIN DU PROGRAMME')
        #sys.exit()
        return
            
    else:
        print("Erreur lors de l'identification du N° d'échantillon !")
        sys.exit()


    session.commit()


    #Il faudra penser a recuperer la liste des callers du projet


    
    #file = open(vcf_file, "r")
    
    with open(vcf_file,"r") as file:
        is_niourk = False
        is_mito = False
        for line in file:
            if re.search('##Nk_calls', line):
                is_niourk = True
            if re.search('chrM.fasta', line):
                is_mito = True

        if(is_niourk):
            if(is_mito):
                caller_list = ['STRELKA','MUTECT2']
            else:
                caller_list = ['GATK', 'STRELKA','DEEPVARIANT']
        else :
            caller_list = ['GATK']
            
            
    # =============================================================================
    # Pretraitement des donnees
    # =============================================================================


    table_output = "./tabledata_" + run_name + "_" +sample_name + ".csv"

    command = 'python3 ' + path_vcfSimplify + " SimplifyVCF -toType table -inVCF " + \
        vcf_file + " -outFile " + table_output
        
    process = subprocess.Popen(command.split(), stdout=subprocess.PIPE)
    process.wait()

    del process

    data = pd.read_csv(table_output, sep='\t', dtype={'CHROM': str})
    if(data[data['CHROM'].str.contains('chr')].empty):
        data['CHROM'] = 'chr' + data['CHROM'].astype(str)



    data = data[data['FILTER'] == 'PASS'].copy()
    data = data.reset_index(drop=True)
    data = getAFmedian(data, vcf_file)

    

    result = splitMultiCaller(data, caller_list)




    result.columns = ['chrom','pos','ref','alt', 'af_corr'] \
        + [(s + "_af").lower() for s in caller_list] \
            + [(s + "_qual").lower() for s in caller_list]



    # # =============================================================================
    # # Database Update
    # # Insertion ou mises a jours des donnees
    # # =============================================================================


    result['pos'] = result['pos'].astype('object')
    # # del data
    chromosomes = list(result['chrom'].unique())

    #initialise output dataframe
    df_for_insert = pd.DataFrame(columns=['var_id','chrom','pos','ref','alt'])
    df_for_update = pd.DataFrame(columns=['var_id','chrom','pos','ref','alt'])
    mask = (result['alt'].str.len() <= 100) & (result['ref'].str.len() <= 100)
    result = result.loc[mask]

    manager = Manager()
    pool = Pool(processes= threadVar)
    
    for chrom in chromosomes:
        print("Add " + chrom + "...")
        df_base = pd.read_sql(session.query(Variant).filter(Variant.chrom==chrom).statement,session.bind)
        chrom_data = result[result['chrom'] == chrom]
        
        df_split = np.array_split(chrom_data,threadVar)
        func = partial(checkVariant, df_base)
        res = pool.map(func, df_split)
        
        #loop in tuples list for assign res in correct dataframe
        for elt in res:
            df_for_insert = df_for_insert.append(elt[0], ignore_index=True)
            df_for_update = df_for_update.append(elt[1], ignore_index=True)
    pool.close()

# =============================================================================
# Une fois le traitement terminee, on met a jours la database
# =============================================================================
    
    print("\nTOTAL :\n")
    
    if( not df_for_insert.empty):
        query = session.execute("SELECT `auto_increment` FROM INFORMATION_SCHEMA.TABLES WHERE table_schema = '" + db_name + "' AND table_name = 'variants';")
        last_primary_key = [{column: value for column, value in rowproxy.items()} for rowproxy in query][0]['auto_increment']-1
        
        session.begin_nested() 
        
        session.bulk_insert_mappings(Variant, df_for_insert.to_dict(orient="records"),return_defaults = True)

        
        query = session.execute("SELECT `auto_increment` FROM INFORMATION_SCHEMA.TABLES WHERE table_schema = '" + db_name + "' AND table_name = 'variants';")
        new_last_primary_key = [{column: value for column, value in rowproxy.items()} for rowproxy in query][0]['auto_increment']-1
        print("\nTemps d execution : %s secondes ---" % (time.time() - start_time))
        #Si on a le même nombre variant dans le tableau d'insertion  que le nombre de cle ajouté par insert alors
        #on ajoute le lien var_id sample dans la table d'association
        if(len(df_for_insert) == (new_last_primary_key - last_primary_key)):
# =============================================================================
#         Ajoute l association variant sample
# =============================================================================
            df_assoc_var_samples = pd.DataFrame()
            df_assoc_var_samples['var_id']=range(last_primary_key+1, new_last_primary_key+1)
            df_assoc_var_samples['sample_id']=s_id
            af_value = df_for_insert['af_corr'].tolist()
            df_assoc_var_samples['af_gatk']=af_value

            #Chunck du tableau pour optimiser les requetes
            for sub_df_assoc_var_samples in chunk_df(df_assoc_var_samples,20000):
                session.bulk_insert_mappings(AssociationVariantSample, 
                                            sub_df_assoc_var_samples.to_dict(orient="records"),return_defaults = True)
                session.commit()
            
            #Preparation de l'insertion dans la table association gene variant
            current_idx = last_primary_key +1

            
            print(str(new_last_primary_key - last_primary_key) + " insertions")
            print("\nTemps d execution : %s secondes ---" % (time.time() - start_time))
    # =============================================================================
    #         Ajout table Call
    # =============================================================================
            
            var_id_list  = list(range(last_primary_key +1, new_last_primary_key +1 ))
            df_for_insert['var_id'] = list(range(last_primary_key +1, new_last_primary_key +1 ))
            # session.commit()
                    
            col_pos = getCallerCol(caller_list, df_for_insert)
            
            manager = Manager()
        
            pool = Pool(processes=threadVar)
            df_split = np.array_split(df_for_insert, threadVar)
            
            func = partial(splitCallTable,col_pos, caller_list,s_idVar=s_id)
            
            
            res = pool.map(func, df_split)
            df_insert_call = pd.DataFrame()
            
            for elt in res:
                df_insert_call = df_insert_call.append(elt, ignore_index=True)
            
            for sub_df_insert_call in chunk_df(df_insert_call,20000):
                session.bulk_insert_mappings(Call, df_insert_call.to_dict(orient="records"),return_defaults = True)
                session.commit()
                
            pool.close()
        else : 
            session.rollback()
            x = session.query(Sample).get(s_id)
            session.delete(x)
            session.commit()
            print("[ERROR] : Fin de la transaction... Annulation des insertions.")
            sys.exit()
        
    
    else:
        print("    0 insertion")

        session.commit()
    


    
# =============================================================================
# les updates 
# =============================================================================
    if ( not df_for_update.empty):
        # print("\nTemps d execution : %s secondes ---" % (time.time() - start_time))
        # df_for_update.reset_index()
        df_add_association = pd.DataFrame()
        af_value = df_for_update['af_corr'].tolist()
        # df_for_update = df_for_update.drop(columns=['csq', 'hgnc_id'])
        session.bulk_update_mappings(Variant, df_for_update.to_dict(orient="records"))
        session.flush()
        df_add_association['var_id'] = df_for_update['var_id'].copy()
        df_add_association['sample_id']=s_id
        df_add_association['af_gatk']=af_value
        
        
        for sub_df_add_association in chunk_df(df_add_association,20000):

            session.bulk_insert_mappings(AssociationVariantSample, sub_df_add_association.to_dict(orient="records"))
            session.commit()
        
        col_pos = getCallerCol(caller_list, df_for_update)
        pool = Pool(processes=threadVar)
        df_split = np.array_split(df_for_update, threadVar)
        
        func = partial(splitCallTable, col_pos, caller_list,s_idVar=s_id)
        
        res = pool.map(func, df_split)
    
        df_insert_call_udpate = pd.DataFrame()
        #loop in tuples list for assign res in correct dataframe
        for elt in res:
            df_insert_call_udpate = df_insert_call_udpate.append(elt, ignore_index=True)
            
        
        for sub_df_insert_call_udpate in chunk_df(df_insert_call_udpate,20000):
        
            session.bulk_insert_mappings(Call, sub_df_insert_call_udpate.to_dict(orient="records"),return_defaults = True)
            session.commit()

        print("    " + str(len(df_for_update)) + " updates")
        pool.close()
            
    else : 
        print("    0 update")

    session.commit()
    os.remove(table_output)
        

    print("\nTemps d execution : %s secondes ---" % (time.time() - start_time))

    gc.collect()

    return



parser = argparse.ArgumentParser(description='Add sample to database.', epilog="NAVET Benjamin & GUERACHER Pierre")
parser = argparse.ArgumentParser()

parser.add_argument('-v', '--version', action='version', version='%(prog)s 6.0')
parser.add_argument("-f", "--file", type=str, help="chemin du fichier No need if excel conf", required=False)
parser.add_argument("-fo", "--folder",type=str,help="Specify folder where all run subfolder are located",required=False)
parser.add_argument("-x","--paramRun",type=str,help="CSV file with FOLDER_NAME on Illumina stuff and name RUN_XXX_EXOME in column ")
parser.add_argument("-c", "--conf", type=str, help="fichier conf", required=True)
parser.add_argument("-e", "--experiment_name", type=str, help="nom de l'experience - No Need if excel conf", required=False)
parser.add_argument("-p", "--type", type=str, default='', help="run type")
parser.add_argument("-d", "--date", type=str, help="date du run- No Need if excel conf", required=False)
parser.add_argument("-s", "--sequenceur", type=int, help="sequenceur", default=2)
parser.add_argument("-i", "--vcfSimp",type=str,default='./tools/VCF-Simplify/VcfSimplify.py')
parser.add_argument("-t", "--thread", type=int, help="nbr thread", default=6 )

args = parser.parse_args()
  
def checkNameFiles(list):
    
    for fileName in list:
        count=len(fileName.split("/")[-1].split("_"))
        if count != 3:
            
            print(f"""Please respect filename nomenclature 
                  RXXX_ID_SX-PASS-final.vcf ({fileName.split("/")[-1]})""")
            sys.exit(1)
        else:
            continue
            
    return True

def getFolderVCF(folder,name):
    
    path =f"{folder}/RUN_{name.split('_')[1]}"
    
    if os.path.exists(path)==False:
        print(f"Your path {path} don't exist")
        sys.exit(1)
    elif os.path.exists(f"{path}/VCF/")==False:
        print(f"Your project don't have VCF folder in {path} ! ")
        sys.exit(1)
    else: 
        
        listVCF=glob.glob(f"{path}/VCF/*vcf")
        
        if checkNameFiles(listVCF):
            
            return listVCF
if __name__ == '__main__':

    if args.paramRun:
        list_specification=getSpecification(args.paramRun)
        
        for date,exp in list_specification:
            
            vcfFiles = getFolderVCF(args.folder,exp)
            
            for vcf in vcfFiles:
                try:db_processing(path_vcfSimplify=args.vcfSimp,vcf_file=vcf,confFile=args.conf,
                            expName=exp,runType=args.type,runDate=date,
                            sequenceurId=args.sequenceur,threadVar=args.thread)
                except Exception as e:
                    print(f"Une erreure est survenue lors de l'import du fichier {vcf}: {e}")
                    sys.exit(1)
    else:
        
        if args.date and args.experiment_name:
            
            try: db_processing(path_vcfSimplify=args.vcfSimp,vcf_file=args.file,confFile=args.conf,
                        expName=args.experiment_name,runType=args.type,runDate=args.date,
                        sequenceurId=args.sequenceur,threadVar=args.thread)
            except Exception as e:
                    print(f"Une erreure est survenue lors de l'import: {e}")
        else: 
            print("If you don't use excel conf, you need to specify date and experiment name")
            sys.exit(1)


