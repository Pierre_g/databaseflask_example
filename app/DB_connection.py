#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun May  3 07:52:21 2020

@author: tigrou
"""

from sqlalchemy.orm import mapper
import sqlalchemy as sa
from sqlalchemy.orm import sessionmaker
from DB_class import Base, Pipeline, Sequenceur, Experiment,\
    Sample, Call, Variant, AssociationVariantSample,\
    Gene, Annotation, Consequence, \
         AssociationVariantAnnotation, User, AssociationVariantGene#,\
            #AssociationAnnotationGene, AssociationVariantCustomAnnotation#, AssociationVariantGene,CustomAnnotation
from DB_method import createGeneTable
from sqlalchemy.orm import scoped_session
from sqlalchemy.inspection import inspect





def tableExit(engine, table,verbose=False):
    if not inspect(engine).dialect.has_table(engine.connect(),table):
        
        print("  [INFO] : CREATE " + table.upper() + " TABLE ...")
        return False
        
    else:
        if verbose == True:print("  [INFO] : " + table.upper() + " TABLE ALREADY EXIST")    
        return True
 
    
def getScopedSession(db_name,db_user,db_pwd):
    #from global_ import db_name, db_user, db_pwd
    engine = sa.create_engine('mysql+mysqlconnector://' + db_user + ':' + db_pwd + '@localhost:3306/' + db_name,
    echo=False, pool_size=10000, connect_args={'connect_timeout': 300}, execution_options={"timeout": 300,
                                             "statement_timeout": 300,
                                             "query_timeout": 300,
                                             "execution_timeout": 300})

    session_factory = sessionmaker(bind=engine)
    Session = scoped_session(session_factory)
    
    return Session
    
def getSession(db_name,db_user,db_pwd,verbose=False):
    # from global_ import db_name, db_user, db_pwd
    if verbose==True:print(f"OBTENTION DE LA CONNEXION VERS {db_name}")
    # print("DB_name : " + db_name)
    engine = sa.create_engine('mysql+mysqlconnector://' + db_user + ':' + db_pwd + '@localhost:3306/' + db_name,
    echo=False, pool_size=10000, connect_args={'connect_timeout': 300}, execution_options={"timeout": 36000,
                                             "statement_timeout": 300,
                                             "query_timeout": 36000,
                                             "execution_timeout": 300,
                                             "pool_recycle":3600})


    Session = sessionmaker()
    Session.configure(bind=engine)
    
    session = Session()
    
    # metadata = MetaData(engine)


    if verbose==True:print("  [INFO] : VERIFICATION DES TABLES...")
    # if(tableExit(engine, 'Variants') == False):
    #    Variant.__table__.create()
    
    
    if(tableExit(engine, 'pipelines') == False):
        Pipeline.__table__.create(engine)
        pip = Pipeline(pipeline_name="NiourkV2")
        session.add(pip)
        session.commit()
    

    if(tableExit(engine, 'sequenceurs') == False):
        Sequenceur.__table__.create(engine)
        seq = Sequenceur(sequenceur_name="Seq_integragen", sequenceur_fournisseur="Illumina", serial_number="")
        session.add(seq)
        seq = Sequenceur(sequenceur_name="NextSeq550", sequenceur_fournisseur="Illumina", serial_number="NB552018")
        session.add(seq)
        seq = Sequenceur(sequenceur_name="MiSeq", sequenceur_fournisseur="Illumina",serial_number="")
        session.add(seq)
        seq = Sequenceur(sequenceur_name="Proton", sequenceur_fournisseur="ThermoFisher",serial_number="")
        session.add(seq)
        session.commit()
        
        
    if(tableExit(engine, 'experiments') == False):
        Experiment.__table__.create(engine)
   
    
    if(tableExit(engine, 'samples') == False):
        Sample.__table__.create(engine)
        
        
    if(tableExit(engine, 'users') == False):
        User.__table__.create(engine)
        u = User(firstname="admin", name="admin")
        session.add(u)
        session.commit()
           
                        
    if(tableExit(engine, 'variants') == False):
        Variant.__table__.create(engine)
        
    if(tableExit(engine, 'calls') == False):
        Call.__table__.create(engine)
        
    if(tableExit(engine, 'consequences') == False):
        Consequence.__table__.create(engine)
        
    if(tableExit(engine, 'asso_Variant_Sample') == False):
        AssociationVariantSample.__table__.create(engine)
        
        
    if(tableExit(engine, 'genes') == False):
        Gene.__table__.create(engine)
        df_gene = createGeneTable()
        session.bulk_insert_mappings(Gene, df_gene.to_dict(orient="records"))
        session.commit()
        unkown = Gene(hgnc_id='000', hgnc_name='?intergenic_variant')
        session.add(unkown)
        session.commit()
        unkown2 = Gene(hgnc_id='999999999', hgnc_name='?no_annotation')
        session.add(unkown2)
        session.commit()
    
    if(tableExit(engine, 'annotations') == False):
        Annotation.__table__.create(engine)
        
    # if(tableExit(engine, 'custom_annotations') == False):
    #     CustomAnnotation.__table__.create(engine)

    if(tableExit(engine, 'asso_Variant_Annotation') == False):
        AssociationVariantAnnotation.__table__.create(engine)
        
    # if(tableExit(engine, 'asso_Annot_Gene') == False):
    #      AssociationAnnotationGene.__table__.create(engine)
         
    if(tableExit(engine, 'asso_Variant_Gene') == False):
         AssociationVariantGene.__table__.create(engine)

    return session


if __name__ == '__main__':
    try: 
        session = getSession()
        print("[DONE] : DATA IS CONNECTED")
    except Exception as erreur: 
        
        print(erreur)
        print("[ERROR] : DATABASE IS MISSING !!")
