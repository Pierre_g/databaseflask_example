# -*- coding:utf-8 -*-

import logging
import os
import sys
from pandas import ExcelWriter
from configparser import RawConfigParser
from DbRequest_modules import DbRequest
import pandas as pd
from flask import Flask, render_template, request, session,send_file
from flask_dropzone import Dropzone
from flask_reverse_proxy_fix.middleware import ReverseProxyPrefixFix
from tqdm import tqdm
import tempfile
import global_
from DB_class import Annotation  # CustomAnnotation,
from DB_class import (AssociationVariantAnnotation, AssociationVariantSample,
                      Consequence, Experiment, Gene, Sample, Variant)
from DB_connection import getSession
from DB_method import helpMessage, reformat_df
import uuid
from form import GeneForm

config = RawConfigParser()


cfg_file=''
if('gunicorn' in sys.argv[0]):
	cfg_file = sys.argv[2]
else:
    
	cfg_file = sys.argv[1]


logger = logging.getLogger('werkzeug') # grabs underlying WSGI logger
handler = logging.FileHandler('kdatabase.log') # creates handler for the log file
logger.addHandler(handler) 
config.read(cfg_file)

app = Flask(__name__)

app.config.update(
    UPLOADED_PATH=config["FLASK"]["UPLOAD_FOLDER"],
    TEMPLATE_REQUEST = config["FLASK"]["TEMPLATE_REQUEST_FILE"],
    STORAGE_RESULTS = config["FLASK"]["STORAGE_RESULTS"],
    # Flask-Dropzone config:
    DROPZONE_ALLOWED_FILE_CUSTOM=True,
    DROPZONE_ALLOWED_FILE_TYPE='.xlsx',
    DROPZONE_MAX_FILES=1,
    DROPZONE_IN_FORM=True,
    DROPZONE_UPLOAD_ON_CLICK=True,
    DROPZONE_UPLOAD_ACTION='upload_query',  # URL or endpoint
    DROPZONE_UPLOAD_BTN_ID='submit',
    DROPZONE_DEFAULT_MESSAGE="Vous pouvez déposer votre fichier de requête ici"
)

dropzone = Dropzone(app)

ReverseProxyPrefixFix(app)
app.config['REVERSE_PROXY_PATH'] = config.get('FLASK','reverse_proxy')
# SECRET_KEY = os.urandom(32)
app.config['SECRET_KEY'] = os.urandom(32)

global_.db_name = config.get('DB','name')
global_.db_user = config.get('DB','user')
global_.db_pwd = config.get('DB','pwd')

# def add_cust_annotations(sess, data, user, sample):
#     list_var_id = []
#     data = data[["CHR","POS","REF","ALT","USER_COMS", "USER_CLASS"]]
#     data = data[data['USER_COMS'].notna()]
#     data = data.groupby(["CHR","POS","REF","ALT"]).first().reset_index()
#     data = data.rename(columns = {'USER_COMS' : 'variant_annotation',
#                               'USER_CLASS' : 'variant_class'})
    
#     data['user_id'] = user
#     data['sample_id'] = sample
#     data['date'] = str(datetime.now())
    
    
#     for elt, row in data.iterrows():
#         var_id = None

#         try : 
#             result_var = sess.query(Variant).filter_by(chrom = row['CHR'], pos = row['POS'],
#                                                        ref = row['REF'], alt = row['ALT'] ).one()
#             sess.commit()
#             var_id=result_var.var_id
            
#         except MultipleResultsFound : 
#             pass
#         except NoResultFound :
#             pass
        
#         list_var_id.append(var_id)
        
    
#     data['var_id'] = list_var_id
#     data = data[data['var_id'].notna()]
    
#     try:
        
#         sess.bulk_insert_mappings(CustomAnnotation, data.to_dict(orient="records"))
#         sess.commit()
#         message='OK'
#     except IntegrityError:
#         sess.rollback()
#         message='Un des enregistrement est deja present'
    
#     return message


@app.context_processor
def var_init():
    return dict(DATABASE_NAME= config.get('FLASK','DATABASE_NAME'))


def consequence_filter():
    """
    Permet de recuperer la liste des consequences pour la filtration
    """

    consequence_list = ['Missense','Intronic','Stop_gain',
                        'Synonym', 'Start_lost','Frameshift',
                        'Splice','Inframe','Protein_altering',
                        'Others'
                        ]
    return consequence_list


@app.route('/fiche_variant/<variant_id_init>/<period_init>/<sampleName_init>',
           methods=['GET', 'POST'])
@app.route('/fiche_variant/<variant_id_init>/<period_init>/',
           defaults={"sampleName_init":""},
           methods=['GET', 'POST'])
@app.route('/fiche_variant/<variant_id_init>/<sampleName_init>',
           defaults={"period_init":""},
           methods=['GET', 'POST'])
@app.route('/fiche_variant/<variant_id_init>/<sampleName_init>',
           defaults={"period_init":""},
           methods=['GET', 'POST'])

@app.route('/fiche_variant/<variant_id_init>/',
           defaults={"period_init":"","sampleName_init":""},
           methods=['GET','POST'])
def fiche_variant(variant_id_init,period_init,sampleName_init):
    """
    Page permettant d'afficher la fiche d'un variant en details :
        - liste des annotations VEP
        - liste des echantillons possedant le variant
        - liste des annotations

    La page peut être interroger :
        - via une requete indirect (lien) 
        - ou direct en fournissant l'id du variant

    Parameters
    ----------
    variant_id : TYPE
    Returns
    Template avec dataframe en html
    -------
    """

    if(request.method == 'GET') or (request.method == 'POST') :

        info_dict = {}
        session_sql = getSession(global_.db_name,global_.db_user,global_.db_pwd)
        ##Requete permettant de connaire le nombre d'echantillon
        nbr_sample = session_sql.query(Sample).count()
        session_sql.commit()   
        #---------------------------------|
        #      INITIALISATION VARIABLES   |
        #---------------------------------|
        
        variant_id = variant_id_init
        period = period_init
        sampleName = sampleName_init
        
        if period != "":
            oldest_date,newest_date = period.split("_")
        
        if len(sampleName)!=0:
            try:sampleNameList = sampleName.split("+")
            
            
            except: sampleNameList = [sampleName]
        else:
            sampleNameList=[]

        #---------------------------------|
        #        REQUETES SAMPLES         |
        #---------------------------------|

        if len(sampleNameList)==0:
        #Selectionne les samples contenant le variant grace a la table d'association AssociationVariantSample

            
            if (period!=""):

                query_sample = pd.read_sql(session_sql.query(Experiment.run_date,Experiment.run_name,
                                                        Sample.sample_number
                                                        ,AssociationVariantSample.af_gatk)
                                        .join(AssociationVariantSample, AssociationVariantSample.sample_id == Sample.sample_id)
                                        .join(Experiment, Experiment.run_id == Sample.run_id)
                                        .filter(AssociationVariantSample.var_id == variant_id)
                                        .filter(Experiment.run_date <= newest_date)
                                        .filter(Experiment.run_date >= oldest_date).statement,session_sql.bind)
            else:

                query_sample = pd.read_sql(session_sql.query(Experiment.run_date,Experiment.run_name,
                                        Sample.sample_number
                                        ,AssociationVariantSample.af_gatk)
                        .join(AssociationVariantSample, AssociationVariantSample.sample_id == Sample.sample_id)
                        .join(Experiment, Experiment.run_id == Sample.run_id)
                        .filter(AssociationVariantSample.var_id == variant_id).statement,session_sql.bind)
            
        if len(sampleNameList)>0:
            

            if period != "":

                query_sample = pd.read_sql(session_sql.query(Experiment.run_date,Experiment.run_name,
                                                        Sample.sample_number
                                                        ,AssociationVariantSample.af_gatk)
                            .join(AssociationVariantSample, AssociationVariantSample.sample_id == Sample.sample_id)
                            .join(Experiment, Experiment.run_id == Sample.run_id)
                            .filter(AssociationVariantSample.var_id == variant_id,
                                    Experiment.run_date <= newest_date,
                                    Experiment.run_date >= oldest_date,
                                    Sample.sample_number.in_(sampleNameList)
                                    ).statement,session_sql.bind)
            else:

                query_sample = pd.read_sql(session_sql.query(Experiment.run_date,Experiment.run_name,
                                                        Sample.sample_number
                                                        ,AssociationVariantSample.af_gatk)
                            .join(AssociationVariantSample, AssociationVariantSample.sample_id == Sample.sample_id)
                            .join(Experiment, Experiment.run_id == Sample.run_id)
                            .filter(AssociationVariantSample.var_id == variant_id,
                                    Sample.sample_number.in_(sampleNameList)
                                    ).statement,session_sql.bind)

        #renome les colonnes
        query_sample.columns=['Date','ID_Run','ID_Echantillon','AF']

        # Recupere les annotations du variant
        query_variant = pd.read_sql(session_sql.query(Variant, Gene, Annotation, Consequence )
                                        .join(AssociationVariantAnnotation, AssociationVariantAnnotation.var_id == Variant.var_id)
                                        .join(Annotation, Annotation.annot_id == AssociationVariantAnnotation.annot_id)
                                        .join(Consequence, Consequence.consequence_id == Annotation.consequence_id)
                                        .join(Gene, Gene.hgnc_id == Annotation.hgnc_id)
                                        .filter(Variant.var_id == variant_id
                                        ).statement,session_sql.bind)

        #dictionnaire utilisee pour resume le variant dans le haut de la page
        info_dict = {'var_id' : query_variant['var_id'].loc[0],
                     'chrom' : query_variant['chrom'].loc[0],
                     'pos' : query_variant['pos'].loc[0],
                     'ref' : query_variant['ref'].loc[0],
                     'alt' : query_variant['alt'].loc[0]
                     }
                
        #extrait un sous dataframe de la requete

        query_variant = query_variant[['hgnc_name', 'HGVSc', "HGVSp",
                                       'impact','consequence_name','CDS_position',
                                        'exon_pos', 'intron_pos','amino_position']]
            
        query_variant.columns=['Gene', 'HGVSc','HGVSp','Impact', 'Consequence',
                               'CDS position', 'Exon', 'Intron', 'Amino']
        
        
        query_variant.drop_duplicates(subset=['HGVSc'], inplace=True)
        
        #Requete permettant de selectionner les annotations customs pour le variant
        # query_custom_annot = pd.read_sql(session.query(CustomAnnotation, func.concat(User.firstname + " " + User.name) , Sample.sample_number)
        #                                  .join(User, User.user_id == CustomAnnotation.user_id)
        #                                  .join(Sample, Sample.sample_id == CustomAnnotation.sample_id)
        #                                  .filter(CustomAnnotation.var_id == int(info_dict['var_id'])).statement,session.bind)
        
        # query_custom_annot = query_custom_annot[['sample_number', 'concat_1', 'variant_annotation', 'variant_class', 'date']]
        # query_custom_annot.columns = ['Sample', 'Ajouté par', 'Commentaire', 'Classe attribué', 'Data']
        #Export en html des dataframes
        #Attention, le nom table_id est important pour l identification par le code javascript
        data_variant = [query_variant.to_html(table_id='table_id', index=False)]
        data_sample = [query_sample.to_html(table_id='table_sample', index=False)]
        # data_custom = [query_custom_annot.to_html(table_id='table_custom', index=False)]
        
        
    
        # return render_template('fiche.html',titre="Fiche_variant",
        #                        table_annotation = data_variant,
        #                        table_sample = data_sample,
        #                        table_custom = data_custom,
        #                        nbr_sample = nbr_sample,
        #                        info_dict = info_dict)
            
        return render_template('fiche.html',titre="Fiche_variant",
                               table_annotation = data_variant,
                               table_sample = data_sample,
                               nbr_sample = nbr_sample,
                               info_dict = info_dict)
      

    return render_template('fiche.html',titre="Fiche_variant",nbr_sample = nbr_sample,
                           info_dict = info_dict)

@app.route('/upload_query', methods=['POST']) 
def upload_query():

    for key, f in request.files.items():
        if key.startswith('file'):
            
            logging.warning(os.path.join(app.config['UPLOADED_PATH'], f.filename))
            f.save(os.path.join(app.config['UPLOADED_PATH'], f.filename))
            session["batch_request"] = os.path.join(app.config['UPLOADED_PATH'], f.filename)
            
        
    return '', 204

    

@app.route('/', methods=['GET', 'POST'])
@app.route('/gene', methods=['GET', 'POST'])
def gene():
    session_sql = getSession(global_.db_name,global_.db_user,global_.db_pwd)
    nbr_sample = session_sql.query(Sample).count()
    
    help_list=helpMessage()
    form = GeneForm()


    list_request = []
    big_error_message = []
    exportRequest = False
    if request.method == 'POST':
        
        if  request.form.get("download_template") == "Export du modèle de requête":
        
            result=send_file(app.config["TEMPLATE_REQUEST"], as_attachment=True)
            return result
        
        if request.form.get("download_request") == "Export":
            exportRequest=True 
        
        if request.form["recherche"] != "":
            try :                                   
                user_query = DbRequest()
            
                user_query.update_criteria(search =request.form["recherche"],
                freqgnomad_min=request.form["freqgnomad_min"],
                freqgnomad_max=request.form["freqgnomad_max"],
                freqCHU_min=request.form["freqCHU_min"],freqCHU_max=request.form["freqCHU_max"],
                samplseStr=request.form["samplename"],period=request.form["period"],
                vaf_htz_min=request.form["vaf_htz_min"], vaf_htz_max=request.form["vaf_htz_max"],
                vaf_hom_min=request.form["vaf_hom_min"],vaf_hom_max=request.form["vaf_hom_max"],
                vaf_lowest_min=request.form["vaf_lowest_min"],vaf_lowest_max=request.form["vaf_lowest_max"],
                occ_tot_min=request.form["occ_tot_min"],occ_htz_min=request.form["occ_htz_min"],
                occ_hom_min=request.form["occ_hom_min"], occ_low_min=request.form["occ_low_min"],
                occ_tot_max=request.form["occ_tot_max"],occ_htz_max=request.form["occ_htz_max"],
                occ_hom_max=request.form["occ_hom_max"], occ_low_max=request.form["occ_low_max"],
                consequence=request.form.getlist('consequence'),
                impact=request.form.getlist('impact'),exportCSV=exportRequest)
                
                list_request.append(user_query)
                
                if session.get("batch_request") != None:
                    os.remove(session["batch_request"])
                session["batch_request"] = None

            except Exception as except_message: 
                return render_template('index.html', form=form,
                                    list_consequence = consequence_filter(),request="error",
                                    message = except_message,
                                    help_message=help_list,effectif=nbr_sample)
        
        elif session.get("batch_request") != None:
            
            try :
                list_request.extend(DbRequest.instantiate_from_excel(session["batch_request"],exportRequest))
                
                

            except Exception as e:
                return render_template('index.html', form=form,
                                        list_consequence = consequence_filter(),
                                        request="error",
                                        message = e,
                                        help_message=help_list,
                                        effectif=nbr_sample)

        
        else:
            message = "Veuillez faire une requête par le formulaire web ou par un lot de requête"
            return render_template('index.html', form=form,
                                        list_consequence = consequence_filter(),
                                        request="error",
                                        message = message,
                                        help_message=help_list,
                                        effectif=nbr_sample)
                 

                        
        logging.warning("Requêtages en cours ...")   
        for user_query in list_request:
            user_query.makeQuery(global_.db_name,global_.db_user,global_.db_pwd)
        logging.warning("Requêtages terminés.") 
            
    
        logging.warning("Filtration en cours ...")
            
        for submitted_query in list_request:
            if submitted_query.passFilter:
                    submitted_query.applyFilter(nbr_sample)

        logging.warning("Filtration terminées")  
               
        list_send = []
        for results_request in list_request:
            
            if results_request.passFilter:
                
                results_request.df_filtered["recherche"]=results_request.search

                list_send.append(results_request.df_filtered)
            else:
                big_error_message.append([results_request.search,results_request.message])
        
        if exportRequest:## Ajouter un onglet pour les messages d'erreur des requêtes
            
            strRand = str(uuid.uuid4())

            writer = ExcelWriter(f"./storage_request/request_{strRand}.xlsx")
            
            if len(big_error_message)!=0:

                df_com = pd.DataFrame(big_error_message,columns=["Recherche","Commentaire"])
                df_com.to_excel(writer, 'Commentaires',index=False)
                
                workbook  = writer.book
                worksheet = writer.sheets['Commentaires']
                                # Add a header format.
                header_format = workbook.add_format({
                    'bold': True,
                    'fg_color': '#ffcccc',
                    'border': 1})
                
                for col_num, value in enumerate(df_com.columns.values):
                    worksheet.write(0, col_num, value, header_format)

                column_len = df_com[value].astype(str).str.len().max()
                # Setting the length if the column header is larger
                # than the max column value length
                column_len = max(column_len, len(str(value))) + 3
                # set the column length
                worksheet.set_column(col_num, col_num, column_len)
                
            
            if len(list_send)!=0:

                df_all_request = pd.concat(list_send,axis=0)
                

                df_to_export_brut = df_all_request[["recherche","HGVSg","HGVSc","HGVSp","intron_pos",
                                                    "exon_pos","sample_number","af_gatk","run_name",
                                                    "run_date","consequence_name","gnomAD_AF",
                                                    "total_freq","htz_freq","hom_freq","low_freq"]]
                
                df_to_export = reformat_df(df_to_export_brut)
                                        
                
                df_to_export.to_excel(writer, 'Requete',index=False)
                
                workbook  = writer.book
                worksheet = writer.sheets['Requete']
                # Add a header format.
                header_format = workbook.add_format({
                    'bold': True,
                    'fg_color': '#AFCB54',
                    'border': 1})
                for col_num, value in enumerate(df_to_export.columns.values):
                    worksheet.write(0, col_num, value, header_format)

                    column_len = df_to_export[value].astype(str).str.len().max()
                    # Setting the length if the column header is larger
                    # than the max column value length
                    column_len = max(column_len, len(str(value))) + 3
                    # set the column length
                    worksheet.set_column(col_num, col_num, column_len)

            writer.save()
            
            result = send_file(app.config["STORAGE_RESULTS"]+f"/request_{strRand}.xlsx", 
                as_attachment=True)
            
            return result

        else:
            
            if len(big_error_message)!=0:
                list_str= []
                for element in big_error_message:
                    
                    list_str.append(" : ".join(element))
                
                message_final = "\n".join(list_str)
                good_query = "error"
            else: 
                message_final=""
                good_query = "done"
            if len(list_send)!=0:
                
                df_all_request = pd.concat(list_send,axis=0)
            
                df_to_web = df_all_request[["var_id","hgnc_name","HGVSg","HGVSc","HGVSp",
                                                "consequence_name","gnomAD_AF","total_freq",
                                                "htz_freq","hom_freq",
                                                "low_freq","total_count"]]
                
                data = [df_to_web.to_html(table_id='table_id', index=False)]
                return render_template('index.html', titre="Result_Gene", 
                                   tables = data ,
                                    form=form, nbr_sample=nbr_sample,
                                    message=message_final,request=good_query,
                                    list_consequence = consequence_filter(),
                                    help_message=help_list,effectif=nbr_sample)
            else:
                return render_template('index.html', titre="Result_Gene", 
                                    form=form, nbr_sample=nbr_sample,request="error",
                                    message=f"{message_final}",
                                    list_consequence = consequence_filter(),
                                    help_message=help_list,effectif=nbr_sample) 
                
    else:
        return render_template('index.html', form=form,
                               list_consequence = consequence_filter(),
                               help_message=help_list,effectif=nbr_sample)



    
# @app.route('/add_annotation', methods=['GET', 'POST'])
# def add_annotation():
    
#     sess = getSession(global_.db_name,global_.db_user,global_.db_pwd)
#     #Recupere la liste des utilisateurs
#     df_users = pd.read_sql(sess.query(User.user_id, User.firstname + " " + User.name).statement,sess.bind)
#     df_users.columns = ['user_id','users']
#     df_users['user_id'] = df_users['user_id'].apply(str)

#     groups_user = [(v['user_id'], v['users']) for i,v in df_users.iterrows()]
#     #Recupere la liste des runs
#     df_run = pd.read_sql(sess.query(Experiment.run_id, Experiment.run_name).statement,sess.bind)
#     df_run = df_run.sort_values(by=['run_id'], ascending=False)
#     df_run['run_id'] = df_run['run_id'].apply(str)
    
#     groups_run = [(v['run_id'], v['run_name']) for i,v in df_run.iterrows()]
#     # groups_run.insert(0, ('0',''))
#     #Recupere la liste des samples
#     df_sample = pd.read_sql(sess.query(Sample.run_id, Sample.sample_id, Sample.sample_number).statement,sess.bind)
#     dict_sample = {}
#     for elt, row in df_sample.iterrows():
#         if row['run_id'] in dict_sample:
#             dict_sample[row['run_id']].update({row['sample_id'] : row['sample_number']})
#         else:
#             dict_sample[row['run_id']] = {row['sample_id'] : row['sample_number']}

#     # dict_sample[0] = {0 : 'All'}
#     form = AnnotationForm()
#     form.operator.choices = groups_user
#     form.run.choices = groups_run
    
#     if request.method == 'POST':
#         operator = request.form["operator"]
#         sample = request.form["sample"]
#         # if(sample == 'All'):
#         #     sample = None
#         file_type = request.form["logiciel"]

#         #On test si la session contient une url pour le fichier
#         try:
#             if(session['file_url'] == ''):
#                 return render_template('addannot.html', form=form, message="Erreur : pas de fichier")
#             else:
#                 filename = app.config['UPLOAD_FOLDER'] + session['file_url']
               
#         except:
#             return render_template('addannot.html', form=form, message="Erreur : pas de fichier")
        
#         # Ajout gestion type fichier : niourk ou sirius
#         df = pd.DataFrame()
           
#         #Cas d'un fichier Sirius
#         message = ""
        
#         if(file_type == "sirius"):
#             file =  open(filename,"r",encoding='latin1')
#             line_number=0
#             for number, line in enumerate(file):
#                 if("User Gene annotation") in line:
#                   line_number = number
#                   print(line_number, file=sys.stdout)
#                   break
#             file.close()
            
#             if(line_number == 0):
#                 return render_template('addannot.html', form=form, message="Erreur")
                
#             df = pd.read_csv(filename, encoding='latin1', skiprows=range(1, line_number), sep="\t", header=1)
#             df = df.rename(columns = {'Chrom' : 'CHR', 'Position' : 'POS',
#                                     'Base ref' : 'REF', 
#                                     'Variant classif annotation':'USER_CLASS',
#                                     'Variant comment' : 'USER_COMS'})
            
#             message = add_cust_annotations(sess, df, operator, sample)
            
#         elif(file_type == 'niourk'):
#             wb = op.load_workbook(filename)
            
#             if(len(wb.sheetnames) > 2):
#                 # print("Biochimie", file=sys.stdout)
#                 df_users = pd.read_sql(sess.query(User.user_id, User.paraphe).statement,sess.bind)
#                 pos = 0
#                 sheet_list = [x.upper() for x in wb.sheetnames]
#                 for name in sheet_list:
#                     value = df_users.loc[df_users['paraphe'] == name]
#                     if(len(value) ==1 ):
#                         operator = value.iloc[0,0]
#                         wb.active = pos
#                         ws = wb.active
#                         df = pd.DataFrame(ws.values)
#                         new_header = df.iloc[0]
#                         df = df[1:]
#                         df.columns = new_header
                        
#                         m = add_cust_annotations(sess, df, operator, sample)
#                         message += name + ' ' + m + '\n'

#                     #pos suit la position dans la liste de excel sheet
#                     pos +=1
#             else:
#                 wb.active = pos
#                 ws = wb.active
#                 df = pd.DataFrame(ws.values)
#                 new_header = df.iloc[0]
#                 df = df[1:]
#                 df.columns = new_header
                
#                 m = add_cust_annotations(sess, df, operator, sample)
                    
                    
                    
#         elif(file_type == 'bulk'):
            
#             wb = op.load_workbook(filename)
#             ws = wb.active
#             df = pd.DataFrame(ws.values)
#             new_header = df.iloc[0]
#             df = df[1:]
#             df.columns = new_header
            
#             message = add_cust_annotations(sess, df, operator, sample)

#         else: 
#             return render_template('addannot.html', form=form, message="Fichier non pris en compte")

        
#         del(session['file_url'])
        
#         return render_template('addannot.html', form=form, message=message, data = dict_sample)
        
#     return render_template('addannot.html', form=form, message="",data = dict_sample)

# @app.route('/upload', methods=['GET', 'POST'])  
# def upload():
#     if request.method == 'POST':
#         f = request.files.get('file')
#         filename = f.save(os.path.join(app.config['UPLOAD_FOLDER'], f.filename))
#         filename = f.filename
#         session["file_url"] = filename
#         return "uploading..."
        
#     return render_template('addannot.html')


if __name__ == '__main__':
    app.run(debug=False, host='127.0.0.1', port='88888')