#!/usr/bin/env python3
# -*- coding: utf-8 -*-


from multiprocessing import Pool, Manager
import numpy as np
import pandas as pd
import time
from joblib import Parallel, delayed
from sqlalchemy.sql import func, text
from sqlalchemy_filters import apply_filters
from datetime import date
from configparser import RawConfigParser
import argparse
import global_
from DB_connection import getSession, getScopedSession
from DB_class import Base, Sample, Variant, AssociationVariantSample,\
    Gene, Annotation, AssociationVariantAnnotation
import tempfile as tmp
config = RawConfigParser()

def req(dbName,dbId,dbPwd,df,variantTab,geneTab,sampleTab,
        assoVarSampleTab,annotTab,assoVarAnnotTab):

    Session = getScopedSession(dbName,dbId,dbPwd) 
    session = getSession(dbName,dbId,dbPwd)
    some_session = Session()
    
    list_var = df.var_id.to_list()
    
    req_sample = (session.query(variantTab.var_id, variantTab.chrom, variantTab.pos, variantTab.ref,\
                               variantTab.alt, geneTab.hgnc_name, sampleTab.sample_number,\
                               assoVarSampleTab.af_gatk,annotTab.annot_id)
        .join(assoVarSampleTab, assoVarSampleTab.var_id == variantTab.var_id)
        .join(sampleTab, assoVarSampleTab.sample_id == sampleTab.sample_id)
        .join(assoVarAnnotTab, assoVarAnnotTab.var_id == variantTab.var_id)
        .join(annotTab, annotTab.annot_id == assoVarAnnotTab.annot_id)
        .join(geneTab, geneTab.hgnc_id == annotTab.hgnc_id)
        .filter(variantTab.var_id.in_(list_var)))
        
    query_sample = pd.read_sql(req_sample.statement,some_session.bind)
    some_session.close()
    Session.remove()
    return query_sample


def Main(outputFile,th,db_name,db_user,db_pwd):
    
    session = getSession(db_name,db_user,db_pwd)
    
    df_sample = pd.read_sql(session.query(Sample).statement,session.bind) 
    nbr_sample  = len(df_sample)

    chromosomes = ['chr1', 'chr2', 'chr3','chr4','chr5','chr6',
                'chr7','chr8','chr9','chr10','chr11','chr12',
                'chr13','chr14','chr15','chr16','chr17','chr18',
                'chr19','chr20','chr21','chr22','chrX','chrY']
    
    for index,chrom in enumerate(chromosomes):
        
        start_time_chr = time.time()
        print(chrom + '...\n')
    
        filter_spec = [{'model' : 'Variant', 'field' : 'chrom', 'op' : '==', 'value' : chrom}]
        
        subquery_variant = session.query(Variant.var_id)
        
        filtered_variant = apply_filters(subquery_variant,filter_spec)
        df = pd.read_sql(filtered_variant.statement,session.bind)
        
        if(len(df) == 0):
            print("Pas de variant pour le " + chrom)
            continue
        
        chunk_size = 10000
        num_chunks = len(df) // chunk_size + 1
        
        df_split = np.array_split(df, num_chunks)
        
        #res = pool.map(req,df_split)
                
        
        
        res=Parallel(prefer='processes',n_jobs=th)(delayed(req)(db_name,db_user,db_pwd,df=tab,variantTab=Variant,geneTab=Gene,sampleTab=Sample,\
                                                                assoVarSampleTab=AssociationVariantSample,\
                                                                annotTab=Annotation,\
                                                                assoVarAnnotTab=AssociationVariantAnnotation) for tab in df_split)
        print("End of multiprocessing")
        

        query_sample = pd.DataFrame()
        
        for elt in res:
            query_sample = query_sample.append(elt, ignore_index=True)

        

        query_sample = query_sample.groupby(["var_id","chrom", "pos","ref",
                                            "alt", "hgnc_name", "annot_id"], 
                                            as_index=False).agg(lambda x: x.tolist())
        

        query_sample['annot_id'] = query_sample['annot_id'].apply(str)
        query_sample['var_id'] = query_sample['var_id'].apply(str)
        query_sample['htz_count'] = ''
        query_sample['hom_count'] = ''
        query_sample['low_freq'] = ''
        query_sample['total'] = ''

        ## Parcours les AF des samples pour compter hte_count, hom_count ...
        for row, elt in query_sample.iterrows():
            count_htz = 0
            count_hom = 0
            count_low_freq = 0
            elt['total'] = len(elt['sample_number'])
            for value in elt['af_gatk']:
                if (value > 0.9):
                    count_hom +=1
                elif ( 0.25 < value <=0.9): 
                    count_htz += 1
                else: 
                    count_low_freq += 1
            elt['hom_count'] = int(count_hom)
            elt['htz_count'] = int(count_htz)
            elt['low_freq'] = int(count_low_freq)


        del(query_sample['sample_number'])
        del(query_sample['af_gatk'])
        query_sample = query_sample.astype({'chrom' : str, 'pos' : int, 'ref' : str, 'alt' : str, 'htz_count' : int,
                                        'hom_count' : int, 'low_freq' : int, 'total' : int})
        
        query_sample['Frequence'] = query_sample['htz_count'] + query_sample['hom_count'] + query_sample['low_freq'] 


        for elt in ['htz_count', 'hom_count', 'low_freq', 'Frequence']:
            query_sample[elt] = round(query_sample[elt] / nbr_sample,4)

        
        query_sample.drop_duplicates(subset=['var_id'], inplace=True)
        del(query_sample['annot_id'])
        del(query_sample['var_id'])
        del(query_sample['hgnc_name'])
        print(f"\nTemps d execution pour {chrom}: %s secondes ---" % (time.time() - start_time_chr))

        

        if index == 0:
            vcf_file = open(outputFile, "w")
            vcf_file.write("##fileformat=VCFv4.2\n")
            vcf_file.write("""##INFO=<ID=FRQ_HTZ,Number=1,Type=Float,Description="htz Allele Freq in database">\n""")
            vcf_file.write("""##INFO=<ID=FRQ_HOM,Number=1,Type=Float,Description="hom Allele Freq in database">\n""")
            vcf_file.write("""##INFO=<ID=FRQ_LOW,Number=1,Type=Float,Description="low Allele Freq in database">\n""")
            vcf_file.write("""##INFO=<ID=FRQ_TOTAL,Number=1,Type=Float,Description="total Allele Freq in database">\n""")
            vcf_file.write("""##contig=<ID=chr1,length=248956422>
##contig=<ID=chr2,length=242193529>
##contig=<ID=chr3,length=198295559>
##contig=<ID=chr4,length=190214555>
##contig=<ID=chr5,length=181538259>
##contig=<ID=chr6,length=170805979>
##contig=<ID=chr7,length=159345973>
##contig=<ID=chr8,length=145138636>
##contig=<ID=chr9,length=138394717>
##contig=<ID=chr10,length=133797422>
##contig=<ID=chr11,length=135086622>
##contig=<ID=chr12,length=133275309>
##contig=<ID=chr13,length=114364328>
##contig=<ID=chr14,length=107043718>
##contig=<ID=chr15,length=101991189>
##contig=<ID=chr16,length=90338345>
##contig=<ID=chr17,length=83257441>
##contig=<ID=chr18,length=80373285>
##contig=<ID=chr19,length=58617616>
##contig=<ID=chr20,length=64444167>
##contig=<ID=chr21,length=46709983>
##contig=<ID=chr22,length=50818468>
##contig=<ID=chrX,length=156040895>
##contig=<ID=chrY,length=57227415>
##contig=<ID=chrM,length=16569>\n""" 
            )
            vcf_file.write("##Date="+ date.today().strftime("%d/%m/%Y")+"\n")
            vcf_file.write("#CHROM	POS	ID	REF	ALT	QUAL	FILTER	INFO	FORMAT\n")
            for t, row in query_sample.iterrows():
                # print(row)
                vcf_file.write(row['chrom'] + '\t' + str(row['pos']) + '\t.\t' + row['ref'] + '\t' + row['alt'] \
                            + '\t.\t.\tFRQ_HTZ=' + str(row['htz_count']) 
                            + ';FRQ_HOM=' + str(row['hom_count']) + ';FRQ_LOW=' + str(row['low_freq']) + ';FRQ_TOTAL=' + str(row['Frequence']) + "\n")
            vcf_file.close()
        else:
            vcf_file = open(outputFile, "a")   
            for t, row in query_sample.iterrows():
                # print(row)
                vcf_file.write(row['chrom'] + '\t' + str(row['pos']) + '\t.\t' + row['ref'] + '\t' + row['alt'] \
                            + '\t.\t.\tFRQ_HTZ=' + str(row['htz_count']) 
                            + ';FRQ_HOM=' + str(row['hom_count']) + ';FRQ_LOW=' + str(row['low_freq']) + ';FRQ_TOTAL=' + str(row['Frequence']) + "\n")
                
            vcf_file.close()
    
    
    return 

parser = argparse.ArgumentParser(description='Create VCF with all database frequencies', epilog="NAVET Benjamin & GUERACHER Pierre")
parser = argparse.ArgumentParser()

parser.add_argument('-v', '--version', action='version', version='%(prog)s 6.0')
parser.add_argument("-c", "--conf", type=str, help="Conf  File with SQL id", required=True)
parser.add_argument("-o", "--output", type=str, help="Name of output file with all database variant and frequencies",
                    required=False,default="db_chu.vcf")
parser.add_argument('-t', '--thread', type=int,default=6)

args = parser.parse_args()

if __name__ == '__main__':
    
    config.read(args.conf)
    global_.db_name = config.get('DB','name')
    global_.db_user = config.get('DB','user')
    global_.db_pwd = config.get('DB','pwd')
    
    
    # Session = getScopedSession()
    
    start_time = time.time()
    
    Main(args.output,args.thread,config.get('DB','name'),config.get('DB','user'),config.get('DB','pwd'))
    
    print("\nTemps d execution : %s secondes ---" % (time.time() - start_time))



