import pandas as pd
from DB_connection import getSession
from DB_class import Base, Sample, Variant, AssociationVariantSample
from joblib import Parallel,delayed
from sqlalchemy import or_, and_
import os
from configparser import RawConfigParser
import argparse
import numpy as np
from tqdm import tqdm

def getFrequencies(df,nbr_sample):
    
    df['var_id'] = df['var_id'].apply(str)
    # df['annot_id'] = df['annot_id'].apply(str)
    
    df['htz_count'] = -1
    df['hom_count'] = -1
    df['low_freq'] = -1
    df['total'] = -1
    
    
    ## Parcours les AF des samples pour compter hte_count, hom_count...
    for index, row in df.iterrows():
        
        row = row.copy()
        count_htz = 0
        count_hom = 0
        count_low_freq = 0
        df.loc[index,'total'] = len(row['sample_number'])
        
        for value in row['af_gatk']:
            
            if (value > 0.9):
                count_hom +=1
            elif ( 0.25 < value <=0.9): 
                count_htz += 1
            else: 
                count_low_freq += 1
        df.loc[index,'hom_count'] = int(count_hom)
        df.loc[index,'htz_count'] = int(count_htz)
        df.loc[index,'low_freq'] = int(count_low_freq)
    
    del(df['af_gatk'])

    
    df = df.astype({'htz_count' : int,'hom_count' : int,
                                        'low_freq' : int, 'total' : int})
    

    df['Frequence'] = df['htz_count'] + df['hom_count']+ df['low_freq']
    for elt in ['htz_count', 'hom_count', 'low_freq','Frequence']:
        df[elt] = round(df[elt] / nbr_sample,4)
    return df  


def varQueryInDB(variantList,dbName,dbUser,dbPwd):
    
    
    session = getSession(dbName,dbUser,dbPwd)
    
    req_var_id = (session.query(Variant.var_id,
                                Variant.chrom,Variant.pos).filter(Variant.pos.in_(variantList)))

    filtered_variant = pd.read_sql(req_var_id.statement,session.bind)
    


    return filtered_variant

def chunk_df(data, nb_row):
    n = round((len(data) / nb_row),1) + 1
    list_df = np.array_split(data, n)

    return list_df
def makeRequest(df,dbName,dbUser,dbPwd):
    
    
    session = getSession(dbName,dbUser,dbPwd)
    requestDB = (session.query(Variant.var_id,Variant.chrom,
                            Variant.pos,Sample.sample_number,
                            AssociationVariantSample.af_gatk,
                            )
    .join(AssociationVariantSample, AssociationVariantSample.var_id == Variant.var_id)
    .join(Sample, AssociationVariantSample.sample_id == Sample.sample_id)
    .filter(Variant.var_id.in_(df["var_id"].to_list())))

    data_request = pd.read_sql(requestDB.statement,session.bind)
    return data_request

def consultDB(listSNP,th,dbName,dbUser,dbPwd):
    
    #Recup pos var
    listVariant=[]
    for variant in listSNP:
        
        chrom,pos=variant.split("-")
        
        listVariant.append(pos)
    
    session = getSession(dbName,dbUser,dbPwd)
    
    
    nbr_sample = session.query(Sample).count()
    
    session.commit()

    all_varId = varQueryInDB(listVariant,dbName, dbUser,dbPwd)
    #all_varId = pd.concat(res,axis=0,ignore_index=True)
    
    all_varId["chr-pos"] = all_varId["chrom"]+"-"+all_varId["pos"]

    var_in_run=all_varId.loc[all_varId["chr-pos"].isin(listSNP),:]
    
    res = Parallel(n_jobs=th)(delayed(makeRequest)(sub_df,
                                               dbName,
                                               dbUser,dbPwd) for sub_df in tqdm(chunk_df(var_in_run,2000)))
    
    
    data_request = pd.concat(res,axis=0,ignore_index=True)
    
    data_request = data_request.groupby(["var_id","chrom","pos"], 
                                        dropna=False,as_index=False).agg(lambda x: list(set(x.to_list())))
    
    
    data_request=getFrequencies(data_request,nbr_sample)
    data_request["chr-pos"] = data_request["chrom"]+"-"+data_request["pos"]

    return data_request


config = RawConfigParser()
parser = argparse.ArgumentParser(description='', epilog="NAVET Benjamin & GUERACHER Pierre")
parser = argparse.ArgumentParser()

parser.add_argument('-v', '--version', action='version', version='%(prog)s 6.0')
parser.add_argument('-p', '--path', type=str)
parser.add_argument('-t', '--thread', type=int)
parser.add_argument('-c', '--conf', type=str)
args = parser.parse_args()
if __name__ == '__main__':
    
    config.read(args.conf)
    db_name = config.get('DB','name')
    db_user = config.get('DB','user')
    db_pwd = config.get('DB','pwd')
    
    tab = pd.read_csv(args.path,sep="\t")
    
    tab_filter = consultDB(tab["chr-pos"],args.thread,
              db_name,db_user,db_pwd)
    dir_input = os.path.dirname(args.path)
    
    name_file = os.path.basename(args.path).split(".")[0]
    tab_filter.to_csv(f"{dir_input}/{name_file}_DBConsult.tsv",
                      sep="\t",index=None)

