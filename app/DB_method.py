#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import ast
import random
import re
import subprocess
import sys
import time
import logging
import pandas as pd


def helpMessage():
    help_list=["Gene: SAMD11","chr-pos-ref-alt : 1-941119-A-G",
               "Transcrit: NM_001385641",
               "HGVSg: 1:g.942451T>C",
               "HGVSc : NM_001385641.1:c.1516T>C",
               "HGVSp : NP_001372570.1:p.Trp506Arg"]

    return help_list
def timer(func):
    def inner(*args, **kwargs):
        t1 = time.time()
        f = func(*args, **kwargs)
        t2 = time.time()
        print ('Runtime : ' + func.__name__ + ' : {0} seconds'.format(t2-t1))
        return f
    return inner


@timer
def checkIfGeneExist(subdata, df_gene):
    list_all_hgnc_id = []
    
    for items, row in subdata.iterrows():
        list_hgnc_id = row['hgnc_id'].split(',')
        for elt in list_hgnc_id:
            list_all_hgnc_id.append(elt.strip())
              
    return(list(set(list_all_hgnc_id) -set(df_gene['hgnc_id'])))

@timer
def createGeneTable():
    gene_data = pd.read_csv('./db/Homo_sapiens.gene_info', sep='\t')
    subset_gene_data = gene_data[['GeneID', 'Symbol']].copy()
    del(gene_data)
    subset_gene_data.columns = ['hgnc_id', 'hgnc_name']

    return subset_gene_data


def zygotieStatus_old(value):
    """
    Return tuple of int in regards of zygotie status

    """
    if(value == 1):
        return 0,1
    if(value == 0.5):
        return 1,0
    else :
        return 0,0
    
    
def zygotieStatus(value):
    """
    Return tuple of int in regards of zygotie status

    """
    if(value >= 0.9):
        return 0,1,0
    if(value < 0.9 and value >= 0.25):
        return 1,0,0
    else :
        return 0,0,1

@timer
def VEP_csq_traitment(df):
    hgnc_id_list=[]
    for items, row in df.iterrows():
        list_CSQ = row['INFO:CSQ'].split(',')
        id_list = []
        for elt in list_CSQ:
            if(elt == '.'):
                #Si pas d'annotation
                id_list.append('0')
            else:
                hgnc_id = elt.split('|')[4]
                if(hgnc_id == ''):
                    id_list.append('0')
                else:
                    id_list.append(hgnc_id)  
            
        hgnc_id_list.append(', '.join(set(id_list)))
    return hgnc_id_list



@timer
def splitMultiCaller(df, caller_list):
    try: 
        #cas d un vcf niourk v2
        df_tmp = pd.DataFrame(df['INFO:CALLAF'].str.split('|',len(caller_list)).tolist(),
                          columns = [ s + "_AF" for s in caller_list])  
    
        df_tmp2 = pd.DataFrame(df['INFO:CALLQUAL'].str.split('|',len(caller_list)).tolist(),
                              columns = [s + "_QUAL" for s in caller_list])  
        
        
        subdata = df[['CHROM', 'POS','REF','ALT','AF_MED']].copy()
        
        result = pd.concat([subdata, df_tmp, df_tmp2], axis=1)
        return result
    except :
        #cas d un fichier classique
        df_tmp = pd.DataFrame(df['INFO:AF'].to_list(), columns = ['GATK_AF'])
        df_tmp2 = pd.DataFrame(df['INFO:QD'].to_list(), columns = ['GATK_QUAL'])
        
        subdata = df[['CHROM', 'POS','REF','ALT','AF_MED']].copy()

        result = pd.concat([subdata, df_tmp, df_tmp2], axis=1)
        return result


@timer
def getCallerCol(caller_list, df):
    col_pos = []
    for caller in caller_list:
        index_af = [i for i, s in enumerate(df.columns) if re.search(caller.lower() + '_af', s)][0]
        index_qual = [i for i, s in enumerate(df.columns) if re.search(caller.lower() + '_qual', s)][0]
        col_pos.append([index_af, index_qual])
    
    return col_pos
    


def getAFmedian(df, vcf_file):
    
    col =  df.columns.to_list()
        
    command = 'bcftools query -l ' + vcf_file
    process = subprocess.Popen(command.split(), stdout=subprocess.PIPE)
    reel_sample_name = process.communicate()[0].decode('UTF-8').rstrip()
    print(reel_sample_name)
    del process
    
    af_corr=[]
    
    try: 
        AD_index = [i for i, s in enumerate(col) if re.search(reel_sample_name + ':AF', s)][0]
        
        for items, row in df.iterrows():
            af_corr.append(round(row[(AD_index)], 2))
    
        df['AF_MED']=af_corr
        
        return df
        
    except IndexError:
        pass
    
    try : 
        AD_index = [i for i, s in enumerate(col) if re.search(reel_sample_name + ':AD', s)][0]
        DP_index = [i for i, s in enumerate(col) if re.search(reel_sample_name + ':DP', s)][0]
        
        for items, row in df.iterrows():
            value = int(row[(AD_index)].split(',')[1]) / row[(DP_index)]
            af_corr.append(round(value, 2))
    
        df['AF_MED']=af_corr
        
        return df
        
    except IndexError: 
        print('Erreur')
        sys.exit()
        
        
def str_to_float(str_float):

        try:
            data = float(str_float.replace(',', '.'))
            return data
        except ValueError:
            data = None
            raise ValueError('Not a valid float value')
    
def getMANETranscrits(df):
    
    
    maneTranscrit = []
    hgvsg_list = []
    for var_id in df.var_id.unique():
        
        sub_df = df.loc[df["var_id"]==var_id,:]
        # logging.warning(sub_df)
        var_Mane_list = sub_df["mane_select"].to_list()
        
        hadManeSelect = False
        for mane in var_Mane_list:
            
            if mane !=None:
                hadManeSelect=True
                maneTranscrit.append(mane)
                
        if hadManeSelect==False:
            hgvsg_set = set(sub_df["HGVSg"].to_list()) #Permet de récupérer un seul element si plusieurs fois le meme element
            hgvsg_list.extend(hgvsg_set)
    

    return df.loc[((df["mane_select"].isin(maneTranscrit))|(df["HGVSg"].isin(hgvsg_list))),:]

def getFrequencies(df,nbr_sample):
    
    df['var_id'] = df['var_id'].apply(str)
    df['annot_id'] = df['annot_id'].apply(str)
    
    df['htz_count'] = -1
    df['hom_count'] = -1
    df['low_count'] = -1
    df['total_count'] = -1
    
    df['htz_freq'] = -1
    df['hom_freq'] = -1
    df['low_freq'] = -1
    df['total_freq'] = -1
    
    
    ## Parcours les AF des samples pour compter hte_count, hom_count...
    for index, row in df.iterrows():
        
        row = row.copy()
        count_htz = 0
        count_hom = 0
        count_low_freq = 0
        df.loc[index,'total'] = len(row['sample_number'])
        
        for value in row['af_gatk']:
            
            if (value > 0.9):
                count_hom +=1
            elif ( 0.25 < value <=0.9): 
                count_htz += 1
            else: 
                count_low_freq += 1
        df.loc[index,'hom_count'] = int(count_hom)
        df.loc[index,'htz_count'] = int(count_htz)
        df.loc[index,'low_count'] = int(count_low_freq)
    
    # del(df['af_gatk'])

    
    df = df.astype({'htz_count' : int,'hom_count' : int,
                                        'low_freq' : int, 'total_count' : int})
    

    df['total_count'] = df['htz_count'] + df['hom_count']+ df['low_count']
    for elt in ['htz', 'hom', 'low','total']:
        df[f"{elt}_freq"] = round(df[f"{elt}_count"] / nbr_sample,4)

    return df
         
def calculAF_corr(df, vcf_file):
    col =  df.columns.to_list()
        
    command = 'bcftools query -l ' + vcf_file
    process = subprocess.Popen(command.split(), stdout=subprocess.PIPE)
    reel_sample_name = process.communicate()[0].decode('UTF-8').rstrip()
    
    del process

    AD_index = [i for i, s in enumerate(col) if re.search(reel_sample_name + ':AD', s)][0]
    DP_index = [i for i, s in enumerate(col) if re.search(reel_sample_name + ':DP', s)][0]

    af_corr=[]
    row_del=[]
    
    for items, row in df.iterrows():
        
        try:
            value = int(row[(AD_index)].split(',')[1]) / row[(DP_index)]
            af_corr.append(round(value, 2))
            
        except ZeroDivisionError:
            print("ligne : " + str(items) + " : division par zero")
            af_corr.append(round(0, 2))
            row_del.append(items)
        
    
    df['af_corr']=af_corr
    
    """ Supprime les lisgnes dont l'AF est egal a 0
    Erreur cause par le split de multivcf laissant les variants absent 
    chez les parents"""
    
    for i in row_del:
        df.drop(df.index[[i]], inplace=True)

    return df


def checkVariant(df_base, df_set):
    """
    Check variant status in database.
    After select query :
        If not exists : add value to dataframe for insert 'df_insert'
        If exists : add value to dataforme for update 'df_update'

    Parameters
    ----------
    df_set : TYPE dataframe of variant

    Returns
    -------
    df_insert : TYPE Dataframe
        DESCRIPTION.
    df_update : TYPE Dataframe
        DESCRIPTION. 

    """
    #Init output dataframe
    df_insert = pd.DataFrame(columns=['var_id','chrom','pos','ref','alt','htz_count','hom_count','lowfreq_count'])
    df_update = pd.DataFrame(columns=['var_id','chrom','pos','ref','alt','htz_count','hom_count','lowfreq_count'])

    for index, row in df_set.iterrows():
        
        ##check presense of variant in db by select

        req = df_base[(df_base.pos == str(row['pos'])) &
                          (df_base.ref == row['ref'])
                          & (df_base.alt == row['alt'] )]
            
        req = req.reset_index()
        
        #if exist : on ajoute le variant a df_update pour le liee a l echantillon
        if(len(req) == 1 ):
            
            variant_to_update = row.to_dict()
            variant_to_update['var_id'] = req['var_id'][0]
            
            df_update = df_update.append(variant_to_update, ignore_index=True)
            
            ## Mettre la mise à jours de VEP ici ? 
        
        #else add new variant    
        elif(len(req) > 1):
            print('erreur')
            print(row)
            break
        
        else:
            new_variant = row.to_dict()
            new_variant['var_id']=None
            df_insert = df_insert.append(new_variant, ignore_index=True)
   
    
    return df_insert, df_update    

              
              
def reformat_df(df):
    
    dicoFinal = {"recherche":[],
                 "HGVSg":[],
                 "HGVSp":[],
                 "HGVSc":[],
                 "Pos. Intronique":[],
                 "Pos. Exonique":[],
                 "Echantillons":[],
                 "Allele Freq":[],
                 "Nom_de_run":[],
                 "Date_du_run":[],
                 "frequence_CHU":[],
                 "htz_freq":[],
                 "hom_freq":[],
                 "low_freq":[],
                 }
    
    for index, row in df.iterrows():

        index_variant = 0
        
        if isinstance(row["sample_number"],list):
            
            listPat = row["sample_number"]
        else:
            listPat = [row["sample_number"]]
            
        if isinstance(row["run_name"],list):
            
            listRunName = row["run_name"]
        else:
            listRunName = [row["run_name"]]
            
        if isinstance(row["run_date"],list):
            
            listRunDate = row["run_date"]
        else:
            
            listRunDate = [row["run_date"]]
            
        if isinstance(row["af_gatk"],list):
                
            listAF = row["af_gatk"]
        else:
            listAF= [row["af_gatk"]]
        
        
        # if len(listPat) != len(listRunName):
          
        while index_variant < len(listPat):
            dicoFinal["recherche"].append(row["recherche"])
            dicoFinal["HGVSg"].append(row["HGVSg"])
            dicoFinal["HGVSc"].append(row["HGVSc"])
            dicoFinal["HGVSp"].append(row["HGVSp"])
            dicoFinal["Pos. Intronique"].append(row["intron_pos"])
            dicoFinal["Pos. Exonique"].append(row["exon_pos"])
            dicoFinal["Echantillons"].append(listPat[index_variant])
            dicoFinal["Nom_de_run"].append(listRunName[index_variant])
            dicoFinal["Date_du_run"].append(listRunDate[index_variant])
            dicoFinal["Allele Freq"].append(listAF[index_variant])
            dicoFinal['frequence_CHU'].append(row["total_freq"])
            
            dicoFinal["htz_freq"].append(row["htz_freq"])
            dicoFinal["low_freq"].append(row["low_freq"])
            dicoFinal["hom_freq"].append(row["hom_freq"])
            
            index_variant+=1
            
    return pd.DataFrame(dicoFinal)          

def checkRequest(search):

    dicoRegex = {"HGVSp":"^[NX]P_[0-9]*.[0-9]*:p[.]",
                 "HGVSc":"^NM_[0-9]*.[0-9]*:c[.]",
                 "HGVSg":"^[0-9 XYM]:g[.]",
                 'feature':"^NM_[0-9]*$",
                 'coord_tiret':"^[0-9]*[-][0-9]*[-][ATCG]*[-][ATCG]*",
                 "hgnc_name":"^[A-Z0-9]{3,10}"
                 }
    
    for type in dicoRegex.keys():
        check=bool(re.match(dicoRegex[type],search))
        
        if check:
            return type
        
    return False



def getRealConsequence(listConsequence):
    
    list_consequence = []
    
    consequence_dict = {"Missense":'missense_variant',"Intronic":'intron_variant',"Stop_gain":'stop_gained',
                        "Synonym":'synonymous_variant', "Start_lost":'start_lost',"Frameshift":'frameshift_variant',
                        "Splice":'splice_variant', "Protein_altering":'protein_altering_variant',
                        "Inframe":'inframe_variant',
                        "Others":'others'
    }
    
    for csq in listConsequence:
        
        list_consequence.append(consequence_dict[csq])
        
    if(len(list_consequence) !=0):
        if('splice_variant' in list_consequence):
            list_consequence.remove('splice_variant')
            list_consequence.extend(('splice_acceptor_variant', 
                                    'splice_donor_variant'))
        if('inframe_variant' in list_consequence):
            list_consequence.remove('inframe_variant')
            list_consequence.extend(('inframe_deletion', 'inframe_insertion'))
        if('others' in list_consequence):
            list_consequence.remove('others')
            list_consequence.extend(('intergenic_variant', 'downstream_gene_variant',
                                    'upstream_gene_variant', '5_prime_UTR_variant',
                                    '3_prime_UTR_variant'))
        
    return list_consequence
def randomStr(lenStr):   
        # The limit for the extended ASCII Character set
        MAX_LIMIT = 255
        
        random_string = ''
        
        for _ in range(lenStr):
            random_integer = random.randint(0, MAX_LIMIT)
            # Keep appending random characters using chr(x)
            random_string += (chr(random_integer))
        return random_string

        



def clean_alt_list(list_):
    list_ = list_.replace(', ', '","')
    list_ = list_.replace('[', '["')
    list_ = list_.replace(']', '"]')
    return list_

if (__name__  == '__main__'):
    df = pd.DataFrame()
    vcf_file='./RUN_TEST/R64_EXA210064_S5-PASS-final.vcf'
