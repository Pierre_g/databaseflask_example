
from email.policy import default
from tempfile import mkdtemp
from joblib import Parallel, delayed
import pandas as pd
import global_
from sqlalchemy.sql import func, text
from sqlalchemy import or_, and_
from sqlalchemy_filters import apply_filters
from DB_connection import getSession,getScopedSession
from DB_class import Base, Sample, Variant, AssociationVariantSample,\
    AssociationVariantAnnotation
import psutil
from tqdm import tqdm
from configparser import RawConfigParser
import argparse
import time
import glob
from asteval import Interpreter
import shutil


def makeRequest(index,listVar,tmpDir,dbName,dbUser,dbPwd):
    
    sessionVar = getSession(dbName,dbUser,dbPwd)
       
    queryVar = (sessionVar.query(Variant.var_id,Variant.chrom,
                    Variant.pos, 
                    Variant.ref,
                    Variant.alt, 
                    Sample.sample_number,
                    AssociationVariantSample.af_gatk)
                    .join(AssociationVariantSample,AssociationVariantSample.var_id == Variant.var_id)
                    .join(Sample,AssociationVariantSample.sample_id == Sample.sample_id)
                    .filter(Variant.var_id.in_(listVar)))
    
    query_variant = pd.read_sql(queryVar.statement,sessionVar.bind)   
    
    
    
    query_variant = query_variant.groupby(["var_id","chrom", 
                                           "pos","ref", "alt"],
                                          as_index=False).agg(lambda x: x.tolist())
    
    query_variant.to_csv(f"{tmpDir}/{index}_query",index=False,sep="\t")
    
    sessionVar.close()
    
    return len(query_variant)

def makeCount(obj,nbr_sample):
    aeval=Interpreter()
    
    df = pd.read_csv(obj,sep="\t")
    
    df['htz_count'] = ''
    df['hom_count'] = ''
    df['total'] = ''
    
    for row, elt in df.iterrows():
        count_htz = 0
        count_hom = 0
        
        df.loc[row,'total'] = len(aeval(elt['sample_number']))
        
        for value in aeval(elt['af_gatk']):
            
            if (value > 0.9):
                count_hom += 1
            else: 
                count_htz += 1
             

        df.loc[row,'hom_count'] = int(count_hom)
        df.loc[row,'htz_count'] = int(count_htz)
    
    
    del(df['sample_number'])
    del(df['af_gatk'])
    
    df = df.astype({'chrom' : str, 'pos' : int, 'ref' : str, 'alt' : str, 'htz_count' : int,
                                      'hom_count' : int, 'total' : int})
    # """create varaft db"""
    
    df['nb_ind_total']=nbr_sample
    
    df.columns = ['mutation_id','chromosome','position','reference','observed',
                              'nb_ind_het','nb_ind_hom', 'nb_ind_all','nb_ind_total']
    
    
    df.drop(df.index[df['observed'].str.contains(',')], inplace=True)
    

    
    # try:
    for row, elt in df.iterrows():
        # if(len(elt['reference']) > 1  and len(elt['observed']) > 1):
        #     print(elt['position'] + ":" + elt['reference'] + " ==> " + elt['observed'])
        
        if(len(elt['reference']) > 1):

            df.loc[row, "position"]= int(elt['position'])+1
            df.loc[row, "reference"]= elt['reference'][1:]
            df.loc[row, "observed"]= "-"

        elif(len(elt['observed']) > 1):

            df.loc[row, "position"]= int(elt['position'])
            df.loc[row, "reference"]= "-"
            df.loc[row, "observed"]= elt['observed'][1:]

    return df


def Main(outputName,thread):
    
    savedir = mkdtemp()
    
    
    session = getSession(global_.db_name,global_.db_user,global_.db_pwd)
    
    print("Count sample ...")
    nbr_sample = session.query(Sample).count()
    nbr_variant = session.query(Variant.var_id).count()
    print("Counting: [V]")
    session.close()
    
    
    
    list_VarId = range(1,nbr_variant,1)
    
    subLists_VarId=[list_VarId[i:i+100] for i in range(0,len(list_VarId),100)]
    
    
    print("Request Variant ...")
    current_process = psutil.Process()
    subproc_before = set([p.pid for p in current_process.children(recursive=True)])
    
    
    
    listLen=Parallel(n_jobs=thread)(delayed(makeRequest)(index,var,savedir,
                                                 global_.db_name,
                                                 global_.db_user,
                                                 global_.db_pwd) for index,var in enumerate(tqdm(subLists_VarId)))
    
    print(f"Nbr variant: {sum(listLen)}")
 
    print("Request Variant: [V]")


    print("Treatment begin")
    listQuery=glob.glob(savedir+"/*query*")
    
    
        
    print("Manipulating data ...")
    list_query_variant = Parallel(n_jobs=thread)(delayed(makeCount)(objQuery,nbr_sample) for objQuery in tqdm(listQuery))
    print("Manipulating data [V]")
    
    nbVar=0
    for tab in list_query_variant:
        nbVar += len(tab)
        
    print(f"Nb Var {nbVar}")
    finalToVaraft = pd.concat(list_query_variant,axis=0)

    print(finalToVaraft.shape)
    finalToVaraft.to_csv(outputName, header=True, index=None, sep='\t')
    shutil.rmtree(savedir)
    
    subproc_after = set([p.pid for p in current_process.children(recursive=True)])
    for subproc in subproc_after - subproc_before:
        #print('Killing process with pid {}'.format(subproc))
        psutil.Process(subproc).terminate()
    return 

parser = argparse.ArgumentParser(description='Create VCF with all database frequencies',
                                 epilog="NAVET Benjamin & GUERACHER Pierre")
parser = argparse.ArgumentParser()

parser.add_argument('-v', '--version', action='version', version='%(prog)s 6.0')
parser.add_argument('-t', '--thread',default=6,type=int)
parser.add_argument("-c", "--conf", type=str, help="Conf  File with SQL id", required=True)
parser.add_argument("-o", "--output", type=str, help="",
                    required=False,default="text_db_varaft.txt")


args = parser.parse_args()

if __name__ == "__main__":
    start = time.time()
    config = RawConfigParser()
    config.read(args.conf)
    global_.db_name = config.get('DB','name')
    global_.db_user = config.get('DB','user')
    global_.db_pwd = config.get('DB','pwd')

    Main(args.output,args.thread)
    end = time.time()
    print("The time of execution of above program is :", end-start)