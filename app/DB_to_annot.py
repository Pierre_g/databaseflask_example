#!/usr/bin/env python2
# -*- coding: utf-8 -*-

import pandas as pd
import argparse
from DB_connection import getSession
from DB_class import Base, Variant
import global_
from configparser import RawConfigParser


def Main(confFile,filename):
    
    config = RawConfigParser()

    config.read(confFile)

    global_.db_name = config.get('DB','name')
    global_.db_user = config.get('DB','user')
    global_.db_pwd = config.get('DB','pwd')


    session = getSession(config.get('DB','name'),config.get('DB','user'),config.get('DB','pwd'))
    # nbr_variant = session.query(Variant).count()
    # nbr_sample = session.query(Sample).count()
    query_variant = pd.read_sql(session.query(Variant).statement,session.bind)

    export_df = query_variant.copy() 
                      
    export_df['genotype'] = export_df['ref'] + '/' + export_df['alt']

    export_df['chrom'] = export_df['chrom'].map(lambda x: x.lstrip('chrom'))
    # export_df['chrom'] = export_df['chrom'].str.replace('chrom', '')
    if filename=="":
        filename = 'export_' + global_.db_name + '_all.txt'
    with open(filename, 'w') as file :
        for elt, row in export_df.iterrows():
            file.write(row['chrom'] + '\t' + row['pos'] + '\t' + \
                    str(row['var_id']) + '\t' + row['ref'] + '\t' + \
                        row['alt'] + '\t.\t.\t.\n')
    return

parser = argparse.ArgumentParser(description='Add sample to database.', epilog="NAVET Benjamin & GUERACHER Pierre")
parser = argparse.ArgumentParser()

parser.add_argument('-v', '--version', action='version', version='%(prog)s 6.0')
parser.add_argument("-c", "--conf", type=str, help="Conf  File with SQL id", required=True)
parser.add_argument("-o", "--output", type=str, help="Name of file with all database variant",
                    required=False,default="")

args = parser.parse_args()

if __name__ == '__main__':
    
    Main(args.conf,args.output)
