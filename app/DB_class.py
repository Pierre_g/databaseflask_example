#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun May  3 07:56:32 2020

@author: tigrou
"""

from sqlalchemy import Table, Column, Date, MetaData, Integer, String, ForeignKey, ForeignKey, Float, Enum
from sqlalchemy import orm, UniqueConstraint
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import backref
import enum
import sys
from configparser import RawConfigParser

Base =  declarative_base()
# Base = automap_base()
from sqlalchemy.orm import relationship
from datetime import datetime

from global_ import db_name, db_user, db_pwd
#print("DB_classe " + db_name)


# db_name = config.get('DB','name')
# print(sys.argv[0])

def _get_date():
    return datetime.now()


def tableExit(engine, table):
    if not engine.dialect.has_table(engine, table):
        print("  [INFO] : CREATE " + table.upper() + " TABLE ...")
        return False
        
    else:
        print("  [INFO] : " + table.upper() + " TABLE ALREADY EXIST")    
        return True
    
    
    
# =============================================================================
# Class definition
# =============================================================================

class EnumRun(enum.Enum):
    
    exome = 1
    tsc_panel = 2
    cmt_panel = 3
    onco_endoc = 4
    mito = 5
    non_def = 0
    

class Pipeline(Base):
    __tablename__ = 'pipelines'
    __table_args__ = {'schema': db_name}
    
    pipeline_id = Column('pipeline_id', Integer, primary_key=True, autoincrement=True)
    pipeline_name = Column('pipeline_name', String(300))
    pipeline_version = Column('pipeline_version', String(300))
    genome_version = Column('genome_version', String(300))
    
    @orm.reconstructor
    def __init__(self, pipeline_id=None, pipeline_name='', pipeline_version='', genome_version=''):
        self.pipeline_id = pipeline_id
        self.pipeline_name = pipeline_name
        self.pipeline_version = pipeline_version
        self.genome_version = genome_version

    
    
class Sequenceur(Base):
    __tablename__ = 'sequenceurs'
    __table_args__ = {'schema': db_name}
    
    sequenceur_id = Column('sequenceur_id', Integer, primary_key=True, autoincrement=True)
    sequenceur_name = Column('sequenceur_name', String(300))
    sequenceur_fournisseur = Column('sequenceur_fournisseur', String(300))
    serial_number = Column('serial_number', String(300))
        
    @orm.reconstructor
    def __init__(self, sequenceur_id=None, sequenceur_name='', sequenceur_fournisseur = '', serial_number = ''):
        self.sequenceur_id = sequenceur_id
        self.sequenceur_name = sequenceur_name
        self.sequenceur_fournisseur = sequenceur_fournisseur
        self.serial_number = serial_number

    

class User(Base):
    __tablename__ = 'users'
    __table_args__ = {'schema': db_name}
    
    user_id = Column('user_id', Integer, primary_key=True, autoincrement=True)
    firstname = Column('firstname', String(300))
    name = Column('name', String(300))
    paraphe = Column('paraphe', String(30))
    # password = Column(Password(256), nullable=False)

        
    @orm.reconstructor
    def __init__(self, user_id=None, firstname='', name = '', paraphe = ''):
        self.user_id = user_id
        self.firstname = firstname
        self.name = name
        self.paraphe = paraphe




    
class Experiment(Base):
        
    __tablename__ = 'experiments'
    __table_args__ = {'schema': db_name}
    
    run_id = Column('run_id', Integer, primary_key=True, autoincrement=True)
    run_name = Column('run_name', String(300))
    run_number = Column('run_number', String(300))
    run_type = Column(Enum(EnumRun))
    run_date = Column(Date, default=_get_date)
    flow_cell = Column('flow_cell', String(300))
    bed_target = Column('bed_target', String(300))
    librairie = Column('librairie', String(300))
     
    sequenceur_id = Column(Integer, ForeignKey(db_name + '.sequenceurs.sequenceur_id'))
    sequenceur = relationship("Sequenceur")

    
    @orm.reconstructor
    def __init__(self, run_id=None, run_name='', run_type=4, run_date = '', sequenceur_id = '',
                 flow_cell = '', run_number = '', bed_target='', librairie=''):
        self.run_id = run_id
        self.run_name = run_name
        self.run_number = run_number
        self.run_type = run_type
        self.run_date = run_date
        self.flow_cell = flow_cell
        self.bed_target = bed_target
        self.librairie = librairie
        
        self.sequenceur_id = sequenceur_id
        
    def viewSample(self):
        print("RUN_ID : " + str(self.run_id) + " RUN_NAME : "  + self.run_name)
    
    



class Consequence(Base):
    
    __tablename__ = 'consequences'
    __table_args__ = {'schema': db_name}
    
    # gene_id = Column('gene_id', Integer)
    consequence_id = Column('consequence_id', Integer, primary_key=True, autoincrement=True)
    consequence_name =  Column('consequence_name', String(300))
    
    # annotation_consequence = relationship('Annotation', secondary='asso_Annotation_Consequence', back_populates='consequence_annotation')


    @orm.reconstructor
    def __init__(self, consequence_id=None, consequence_name=''):
        # self.gene_id = gene_id
        self.consequence_id = consequence_id
        self.consequence_name = consequence_name





class Sample(Base):
            
    __tablename__ = 'samples'
    __table_args__ = {'schema': db_name}
        
    sample_id = Column('sample_id', Integer, primary_key=True, autoincrement=True)
    #family_id = Column('family_id', String(1000))
    sample_number = Column('sample_number', String(1000))
    sample_status = Column('sample_status', String(1000))
    run_id = Column(Integer, ForeignKey(db_name + '.experiments.run_id'))
   
    run = relationship("Experiment", back_populates='samples')
    
    Experiment.samples = relationship("Sample", back_populates="run")
    
    variant = relationship('Variant', secondary='asso_Variant_Sample', back_populates='sample')
    
    
    @orm.reconstructor
    def __init__(self, sample_id=None, sample_number='', run_id=''):
        self.sample_id = sample_id
        self.sample_number = sample_number
        self.run_id = run_id
        
    def viewSample(self):
        print("SAMPLE_ID : " + str(self.sample_id) + "\nSAMPLE_NAME : " +
              self.sample_number + "\nRUN_ID :"  + str(self.run_id))
        
        
        
        
class Call(Base):
    __tablename__ = 'calls'
    __table_args__ = {'schema': db_name}
    
    call_id = Column('call_id', Integer, primary_key=True, autoincrement=True)
    caller_name = Column('caller_name', String(300))
    af = Column('af', String(300))
    qual = Column('qual', String(300))
    
    
    
    var_id = Column('var_id', Integer,  ForeignKey(db_name + '.variants.var_id'), primary_key=True)
    sample_id = Column('sample_id', Integer,  ForeignKey(db_name + '.samples.sample_id', ondelete='CASCADE'), primary_key=True)
    
    
    pipeline_id = Column('af', String(300))
    pipeline_id = Column(Integer, ForeignKey(db_name + '.pipelines.pipeline_id'))
    
    sample = relationship("Sample")
    variant = relationship("Variant")
    pipeline = relationship("Pipeline")
    

class Variant(Base):
    __tablename__ = 'variants'
    __table_args__ = {'schema': db_name}
    
    
    var_id = Column('var_id', Integer, primary_key=True, autoincrement=True)
    chrom  = Column('chrom', String(10))
    pos = Column('pos', String(150))
    ref = Column('ref', String(1500))
    alt = Column('alt', String(1500))

    sample = relationship('Sample',
                          secondary='asso_Variant_Sample',
                          back_populates='variant')
    
    annotation_variant = relationship('Annotation', secondary='asso_Variant_Annotation',
                                      back_populates='variant_annot')
    variant_gene = relationship('Gene',
                                secondary='asso_Variant_Gene',
                                back_populates='variant_gene')
    # c_annotation_variant = relationship('CustomAnnotation', secondary='asso_Variant_Custom_Annotation', back_populates='variant_custom_annot')
    
    @orm.reconstructor  
    def __init__(self, var_id=None, chrom='',pos=''
                 ,ref='',alt='', htz_count=0, hom_count=0, lowfreq_count=0):
        self.var_id = var_id
        self.chrom = chrom
        self.pos = pos
        self.ref = ref
        self.alt = alt
        # self.samples_id = sample_list

    def viewVariant(self):
        print("Chromosome : " + self.chrom +
              "\nPosition : " + str(self.pos) +
              "\nRef : " + self.ref +
              "\nAlt : "+ self.alt )



def _get_date():
    return datetime.datetime.now()

# class CustomAnnotation(Base):
#     __tablename__ = 'custom_annotations'
#     __table_args__ = (UniqueConstraint('var_id', 'user_id', 'sample_id','date', name='_custum_annot_constr'),{'schema': db_name} )
    
#     c_annot_id = Column('c_annot_id', Integer, primary_key=True, autoincrement=True)
    
#     variant_class = Column('variant_class', String(150))
#     variant_annotation = Column('variant_annotation', String(300))
#     date = Column(Date, default=_get_date)
#     var_id = Column(Integer, ForeignKey(db_name+'.variants.var_id'))
#     user_id = Column(Integer, ForeignKey(db_name+'.users.user_id'))
#     sample_id = Column(Integer, ForeignKey(db_name+'.samples.sample_id'))
    
    
    
#     variant_custom_annot = relationship('Variant', secondary='asso_Variant_Custom_Annotation', back_populates='c_annotation_variant')

  
      
    # @orm.reconstructor
    # def __init__(self, c_annot_id='', variant_class='',variant_annotation='', date=''):
    #     self.c_annot_id = c_annot_id
    #     self.variant_class = variant_class    
    #     self.variant_annotation = variant_annotation
    #     self.date = date
          
  
  
    
class Gene(Base):
    
    __tablename__ = 'genes'
    __table_args__ = {'schema': db_name}
    
    hgnc_id = Column('hgnc_id', Integer, primary_key=True, autoincrement=False)
    hgnc_name =  Column('hgnc_name', String(300))
    variant_gene = relationship('Variant', secondary='asso_Variant_Gene',
                                back_populates='variant_gene',
                                cascade="all,delete", passive_deletes=True)

    @orm.reconstructor
    def __init__(self, hgnc_id='', hgnc_name=''):
        self.hgnc_id = hgnc_id
        self.hgnc_name = hgnc_name     


class Annotation(Base):
    __tablename__ = 'annotations'
    __table_args__ = {'schema': db_name}
    
    
    annot_id = Column('annot_id', Integer, primary_key=True, autoincrement=True)
    vep_version = Column('vep_version', String(150))
   
    impact = Column('impact', String(150))
    feature = Column('feature', String(150))
    feature_type = Column('feature_type', String(150))
    exon_pos = Column('exon_pos', String(150))
    intron_pos = Column('intron_pos', String(150))
    CDS_position = Column('CDS_position', String(150))
    amino_position = Column('amino_position', String(150))
    
    hgnc_id = Column(Integer, ForeignKey(db_name+'.genes.hgnc_id', ondelete='CASCADE'))
    consequence_id = Column(Integer, ForeignKey(db_name+'.consequences.consequence_id'))
    HGVSg = Column('HGVSg', String(150))
    HGVSc = Column('HGVSc', String(150))
    HGVSp = Column('HGVSp', String(150))
    gnomAD_AF = Column('gnomAD_AF', Float(6))
    mane_select = Column('mane_select', String(150))
    mane_clinical = Column('mane_clinical', String(150))
    variant_annot = relationship('Variant', secondary='asso_Variant_Annotation', back_populates='annotation_variant', cascade="all,delete", passive_deletes=True)

    @orm.reconstructor  
    def __init__(self, annot_id=None, vep_version='',impact=''
                 ,feature_type='', feature = '', 
                 exon_pos ='', intron_pos = '', CDS_position = '',
                 amino_position = '',HGVSg='',HGVSc='',HGVSp=''
                 ,gnomAD_AF=0):
        self.annot_id = annot_id
        self.vep_version = vep_version
        self.impact = impact
        self.feature = feature
        self.feature_type = feature_type
        self.exon_pos = exon_pos
        self.intron_pos = intron_pos
        self.CDS_position = CDS_position
        self.amino_position = amino_position
        
        self.HGVSg = HGVSg
        self.HGVSc = HGVSc
        self.HGVSp = HGVSp
        self.gnomAD_AF = gnomAD_AF
        
         

# =============================================================================
# Table d'association 
# =============================================================================


class AssociationVariantSample(Base):
    __tablename__ = 'asso_Variant_Sample'
    # __table_args__ = {'schema': db_name}
    
    var_id = Column('var_id', Integer,  ForeignKey(db_name + '.variants.var_id', ondelete='CASCADE'), primary_key=True)
    sample_id = Column('sample_id', Integer,  ForeignKey(db_name + '.samples.sample_id', ondelete='CASCADE'), primary_key=True)
    af_gatk = Column('af_gatk', Float)
    #pipeline_version
    

class AssociationVariantAnnotation(Base):
    __tablename__ = 'asso_Variant_Annotation'
    # __table_args__ = {'schema': db_name}
    
    var_id = Column('var_id', Integer,  ForeignKey(db_name + '.variants.var_id', ondelete='CASCADE'), primary_key=True)
    annot_id = Column('annot_id', Integer, ForeignKey(db_name + '.annotations.annot_id', ondelete='CASCADE'), primary_key=True)
    
    
# class AssociationVariantCustomAnnotation(Base):
#     __tablename__ = 'asso_Variant_Custom_Annotation'
#     # __table_args__ = {'schema': db_name}
    
#     var_id = Column('var_id', Integer,  ForeignKey(db_name + '.variants.var_id', ondelete='CASCADE'), primary_key=True)
#     c_annot_id = Column('c_annot_id', Integer, ForeignKey(db_name + '.custom_annotations.c_annot_id'), primary_key=True)
    
# class AssociationSampleCustomAnnotation(Base):
#     __tablename__ = 'asso_Sample_Custom_Annotation'
#     # __table_args__ = {'schema': db_name}
    
#     sample_id = Column('sample_id', Integer,  ForeignKey(db_name + '.samples.sample_id', ondelete='CASCADE'), primary_key=True)
#     c_annot_id = Column('c_annot_id', Integer, ForeignKey(db_name + '.custom_annotations.c_annot_id'), primary_key=True)
    
class AssociationVariantGene(Base):
    __tablename__ = 'asso_Variant_Gene'
    # __table_args__ = {'schema': db_name}
    
    var_id = Column('var_id', Integer,  ForeignKey(db_name + '.variants.var_id', ondelete='CASCADE'),primary_key=True)
    hgnc_id = Column('hgnc_id', Integer, ForeignKey(db_name + '.genes.hgnc_id', ondelete='CASCADE'), primary_key=True)
    
    
    
    
if __name__ == '__main__':
    import sqlalchemy as sa
    from sqlalchemy.orm import sessionmaker
    print("OBTENTION DE LA CONNEXION...")
    
    config = RawConfigParser()
    config.read('app.cfg')

    db_name = config.get('DB','name')
    db_user = config.get('DB','user')
    db_pwd = config.get('DB','pwd')
    engine = sa.create_engine('mysql+mysqlconnector://' + db_user + ':' + db_pwd + '@localhost:3306/' + db_name,
    echo=False, pool_size=10)


    Session = sessionmaker()
    Session.configure(bind=engine)
    
    session = Session()
    
    # metadata = MetaData(engine)


    print("  [INFO] : VERIFICATION DES TABLES...")
    # if(tableExit(engine, 'Variants') == False):
    #    Variant.__table__.create()
    
    if(tableExit(engine, 'experiments') == False):
        Experiment.__table__.create(engine)
   
    
    if(tableExit(engine, 'samples') == False):
        Sample.__table__.create(engine)
           
        
    if(tableExit(engine, 'variants') == False):
        Variant.__table__.create(engine)
        
    if(tableExit(engine, 'asso_Variant_Sample') == False):
        AssociationVariantSample.__table__.create(engine)
        
        
    e1 = Experiment(run_name='R14')  
    session.add(e1)
    session.commit()
    r_id = e1.run_id
    
    
    s1 = Sample(sample_number='EXA6528', run_id=r_id)
    session.add(s1)
    session.commit()
    s1.viewSample()
    
    
    
    var1 = Variant(chrom='1', pos='10', alt='A')
    session.add(var1)
    session.commit()
    
    ass = AssociationVariantSample(var_id=var1.var_id, sample_id=s1.sample_id )
    session.add(ass)
    session.commit()
    
    var1 = Variant(chrom='1', pos='30', alt='T')
    session.add(var1)
    session.commit()
    
    ass = AssociationVariantSample(var_id=var1.var_id, sample_id=s1.sample_id)
    session.add(ass)
    session.commit()
    
    var1 = Variant(chrom='11', pos='30', alt='T')
    session.add(var1)
    session.commit()

    
    ass = AssociationVariantSample(var_id=var1.var_id, sample_id=s1.sample_id)
    session.add(ass)
    session.commit()
