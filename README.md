## Procédure de mise à jour de la base de données

__1) Récupération de la base de donnée utilisée sous format .sql__
     `mysqldump -u benjamin -p $DB_NAME > file_date.sql`
Notes:
• Attention cette commande ne fonctionne pas dans l’environnement conda kdatabase.

• Le mot de passe est soit ln17i** soit lni** Il peut varier selon la machine sur
laquelle vous êtes.

• Bien spécifier la date de la version dans le fichier .sql ! Important pour s’y retrouver entre les
différentes versions.

• Pour connaître le nom de la base de donnée ($DB_NAME) faites les commandes suivantes:

    `➢ bash: mysql dump -u benjamin -p`

    `➢ mysql: show databases;` (Liste toutes les base sde données présentes sur le serveur)

__2) Transférer les données sur un autre serveur (K-exome) pour faire la mise à jour.__

__3) Supprimer la base de données du serveur dédié à la mise à jour.__

    `➢ bash: mysql -u benjamin -p`

    `➢ mysql: DROP DATABASE $DB_NAME;`

__4) Créer une base de données vide et importez le fichier .sql__

    `➢ mysql: CREATE DATABASE $DB_NAME;`

    `➢ bash: mysql -u benjamin -p $DB_NAME < file_date.sql`

__5) Identifier le dernier run importé__

    `➢ mysql: SELECT * FROM EXPERIMENTS;`

__6) Récupérer et renommer les VCFs__

A chaque run les fichiers VCF sont conservés dans un dossier backupBioinfo sur SL150, puis une fois
de temps, ces fichiers sont transférés sur k_exome dans le dossier data/share_bioinfo/documents_exomes/RUN_ANGERS/.
Chaque dossier correspond à un run, et les fichiers VCF vont devoir être renommé de la manière
suivante: R$NUMRUN_IDECH_INDEX-PASS-final.vcf.

Par exemple:

• 130918A1_S2-PASS-final.vcf devient R97_130918A1_S2-PASS-final.vcf
Avant de faire la mise à jour de la base de données, assurez-vous que les fichiers séquencés soient
correct (contamination, pas de problème de librairies pendant le séquençages, reséquençage pour
quelques raisons que ce soit.) Vérifiez l’EXCEL des patients sous G:\NGS-EXOME
Pour renommer les fichiers, il est préférable de monter le dossier en volume sur un Ubuntu afin de
pouvoir utiliser la commande rename.

    `➢ Bash:rename -n 's/^/R97_/g' *vcf`

L’option -n permet de vérifier que le regex soit correct avant le renommage définitif.

Si vous devez renommer toute une série de fichier de plusieurs runs:

    `➢ for folder in \"find /data/share_bioinfo/documents_exomes/RUN_ANGERS/ -name *vcf\";do dirname="${folder%/*}" ;NAME=$(basename $folder) RUNNUMBER=$(echo $folder | sort | cut -d "/" -f10 | cut -d "_" -f2);cd $dirname; rename "s/"^"/"R${RUNNUMBER}_"/g" $NAME ;done`

__7) Import des fichiers VCF dans la base de donnée.__
Allez dans le dossier kdatabase (git clone https://gitlab.com/bioinfoconstitangers/kdatabase.git).
Modifiez le fichier de configuration (app_$DB_NAME.cfg) de manière à ce que les identifiants
coïncident avec ceux utilisés sur le serveurs. Puis utilisez le scripts DB_processing.py pour faire
l’import des données.

Notes:

• Le script DB_processing demande à ce que la date soit spécifiée de la manière suivante:
AAAAMMJJ

*Deux méthodes possibles:*

 - A- Faire une boucle et importer les fichier un par un:

    `➢ bash:cd ~/depot_GIT/kdatabase/`
    `➢ bash: for vcf in "ls /data/share_bioinfo/documents_exomes/RUN_ANGERS/RUN_097/VCF/*vcf";do echo "python3 DB_processing.py -f $vcf -e RUN_097_EXOME -p exome -d 20210917 -s 2 -t 30 -c app_exome.cfg";python3 DB_processing.py -f $vcf -e RUN_097_EXOME -p exome -d  20210917 -s 2 -t 30 -c app_exome.cfg;done`

 - B- Créer un tsv contenant les date de run et le nom du run:
• Creation du fichier

• Reprendre la base du fichier RUNS_ILLUMINAS sur G:/NGS-EXOME
• Créer une colonne FOLDER_NAME et NAME

◦ Dans la colonne FOLDER_NAME permet de mettre le nom du dossier brut de chacun des
run (exemple: 210920_NB552018_0126_AHVF5YBGXJ)

◦ La colonne NAME correspond à l’argument -e du script, elle correspond au nom du projet
(exemple: RUN_098_EXOME)

• Le script va diviser le nom du dossier de manière à récupérer la date de séquençage et lancer
DB_processing.py séquentielle. A la moindre erreur, le script s’arrête, il faut donc être
vigilant

    `➢ DB_processing.py -fo /data/share_bioinfo/documents_exomes/RUN_ANGERS/ -p exome -s 2 -t  30 -c app_exome.cfg -x example_excel_conf.csv`

• Si vous avez un problème lors de l’import, vous deez supprimer l’échantillon directement dans
la base de donnée.

    `➢ mysql:mysql -u benjamin -p`

    `➢ mysql:use exome`

    `➢ mysql:select * from samples`

    `➢ mysql:delete from samples where sample_id=XXXX`

__8) Récupération de l’ensemble des variant pour annotation__

    `➢ python3 DB_to_annot.py -c app_exome.cfg`

Par defaut sort un fichier export_$DB_NAME.txt mais peut spécifier un nom de fichier avec arg -o

__9) Annoter les variant avec VEP__

    `> /home/benjaming/tools/ensembl-vep/vep --cache --dir_cache /home/benjaming/kexome/home/exome/.vep/ --fasta /home/benjaming/kexome/home/exome/.vep/homo_sapiens_refseq/106_GRCh38/Homo_sapiens.GRCh38.dna.toplevel.fa --force_overwrite --fork 18 --input_file /home/benjaming/Documents/dev_CHU/kdatabase/subTestHGVS_gnomad.txt --offline --output_file /home/benjaming/Documents/dev_CHU/kdatabase/testWithMANE.vcf --refseq --vcf --vcf_info_field CS --af_gnomad --hgvs --hgvsg --mane --mane_select`

__10) Réimport des variant dans la base de donnée.__

• Suppression des tables d’annotation
    `➢ drop table asso_Annot_Gene asso_Annotation_Consequence asso_Variant_Annotation;`

    `➢ drop table annotations;`

    `➢ python3 ./DB_from_VEP.py -c ../conf/app_exome.cfg -v ./varAnnot_test_db.vcf -e VEP106`

__11) Transfert de la base de donnée sur les serveurs cibles__

    `➢ bash: mysqldump -u benjamin -p $DB_NAME > file_date.sql`

    `➢ mysql: CREATE DATABASE $DB_NAME;`

    `➢ bash: mysql -u benjamin -p $DB_NAME < file_date.sql`

__12) Recharge l’application web__

    `➢ systemctl stop db_exome.service`

    `➢ [Modifier les fichier de conf pour pointer vers la nouvelle base de donnée]`
    
    `➢ systemctl start db_exome.service`


## Dependencies
Pandas >= 1.1

Flask-Dropzone 1.6.0
